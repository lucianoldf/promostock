<?php

class User_model  extends CI_Model
{


    function __construct()
    {
        parent::__construct();

        //Create connect to mongo_db
        $this->mdb->connect();
    }


    function get_user_auth($email, $password)
    {
        $result = $this->mdb->where(['email' => $email, 'password' => $password])->getOne('users');

        return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
    }


    function get_all()
    {
        $result = $this->mdb->get('users');
        return ($result) ? array_to_object($result) : FALSE;
    }


    function get_by_id($id)
    {
        $objecId = $this->mdb->create_document_id($id);

        $result = $this->mdb->where(['_id' => $objecId])->getOne('users');
        return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
    }



    function get_by_email($email)
    {
        $result = $this->mdb->where(['email' => (string) $email])->getOne('users');
        return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
    }



    function create($data)
    {
        $data['_id'] = $this->mdb->create_document_id();
        $newId = $this->mdb->insert('users', $data);

        return $newId;
    }



    function delete($id)
    {

        $objecId = $this->mdb->create_document_id($id);

        $result = $this->mdb->where(['_id' => $objecId])->delete('users');

        return $result;
    }


    function update($id, $data)
    {
        $objecId = $this->mdb->create_document_id($id);

        foreach ($data as $key => $value) {
            $this->mdb->set($key, $value);
        }

        $result = $this->mdb->where('_id', $objecId)->update('users');

        return $result;
    }



    function validate_password($id, $pwd)
    {
        $objectId = $this->mdb->create_document_id($id);

        $result = $this->mdb->where(['_id' => $objectId, 'password' => md5($pwd)])->getOne('users');

        return (count($result) >= 1) ? TRUE : FALSE;
    }
}
