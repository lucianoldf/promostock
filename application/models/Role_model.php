<?php

class Role_model extends CI_Model
{

    function __construct()
    {

        parent::__construct();

        //Create connect to mongo_db
        $this->mdb->connect();
    }



    function get_all($conditions = [], $get_as_row = FALSE)
    {

        $result = $this->mdb->get('roles');
        return ($result) ? array_to_object($result) : FALSE;
    }
}
