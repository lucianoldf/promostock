<?php

class Category_product_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        
        //Create connect to mongo_db
        $this->mdb->connect();
    }



    function get_all()
    {
        $result = $this->mdb->get('categorys_product');
        return ($result) ? array_to_object($result) : FALSE;
    }



    function get_by_id($id)
    {
        $objecId = $this->mdb->create_document_id($id);
        
        $result = $this->mdb->where(['_id' => $objecId])->getOne('categorys_product');
        return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
    }



    function create($data)
    {
        $data['_id'] = $this->mdb->create_document_id();
        $new_id = $this->mdb->insert('categorys_product',$data);
        return $new_id;
    }



    function update($id, $data)
    {
        $objecId = $this->mdb->create_document_id($id);
        
        foreach ($data as $key => $value) {
            $this->mdb->set($key, $value);
        }

        $result = $this->mdb->where('_id', $objecId)->update('categorys_product');

        return $result;
    }



    function delete($id)
    {
        $objecId = $this->mdb->create_document_id($id);

        $result = $this->mdb->where(['_id' => $objecId])->delete('categorys_product');

        return $result;
    }
}
