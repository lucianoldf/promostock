<?php

class Logo_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

        //Create connect to mongo_db
        $this->mdb->connect();
    }



    function get_all()
    {
        $result = $this->mdb->get('logos');
        return ($result) ? array_to_object($result) : FALSE;
    }



    function get_actives()
    {
        $result = $this->mdb->where(['is_active' => 1])->get('logos');
        return ($result) ? array_to_object($result) : FALSE;
    }



    function get_by_section($section)
    {
        $result = $this->mdb->where(['is_active' => 1, 'section' => $section])->getOne('logos');
        return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
    }



    function get_by_id($id)
    {
        $objecId = $this->mdb->create_document_id($id);

        $result = $this->mdb->where(['_id' => $objecId])->getOne('logos');
        return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
    }



    function create($data)
    {
        $data['_id'] = $this->mdb->create_document_id();
        $new_id = $this->mdb->insert('logos', $data);
        return $new_id;
    }



    function disable_all()
    {
        $this->mdb->set('is_active', 0)->updateAll('logos');
    }



    function update($id, $data)
    {
        $objecId = $this->mdb->create_document_id($id);

        foreach ($data as $key => $value) {
            $this->mdb->set($key, $value);
        }

        $result = $this->mdb->where('_id', $objecId)->update('logos');

        return $result;
    }



    function delete($id)
    {
        $objecId = $this->mdb->create_document_id($id);

        $result = $this->mdb->where(['_id' => $objecId])->delete('logos');

        return $result;
    }
}
