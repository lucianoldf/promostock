<?php

class LoadResource {
	protected $CI;



	public function __construct($rules = array())
	{
		$this->CI =& get_instance();

		//$this->load->model('Logo_model', 'logo');

	}

	public function get_logo_resources()
	{
		$this->CI->session->unset_userdata('logos');
		if (!$this->CI->session->has_userdata('logos')) {
			$logo_resources = $this->CI->logo->get_actives();

			$logos = [
				'header' => array_to_object([
					'text' => 'Promostock', 
					'resource' => './assets/logos/logo.png'
					]),
				'shrinking-navbar' => array_to_object([
					'text' => 'Promostock', 
					'resource' => './assets/logos/logo-footer.png'
					]),
				'footer' => array_to_object([
					'text' => 'Promostock', 
					'resource' => './assets/logos/logo-footer.png'
					])
			];

			if ($logo_resources) {

				foreach ($logo_resources as $logo) {
					switch ($logo->section) {
						case 'header':
							$logos['header'] = $logo;
							break;
						case 'shrinking-navbar':
							$logos['shrinking-navbar'] = $logo;
							break;
						case 'footer':
							$logos['footer'] = $logo;
							break;
					}
				}
			}

			$this->CI->session->set_userdata('logos', $logos);
		}
	}
}