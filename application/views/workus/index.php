<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('workus_config_lang'); ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('edit_form_generic_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('edit_form_generic_lang'); ?></h3>
                    </div>
                    <div class="box-body">

                        <?php if ($workus_object->_id != null) {
                            $action = 'workus/update';
                        } else {
                            $action = 'workus/add';
                        } ?>

                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart($action); ?>
                        <input type="hidden" name="workus-id" value="<?= $workus_object->_id; ?>">
                        <input type="hidden" name="old-file" value="<?= $workus_object->resource; ?>">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label><?= translate("form_label_title_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="text" maxlength="255" class="form-control" name="title" placeholder="<?= translate('form_ph_title_lang'); ?>" value="<?= $workus_object->title; ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><?= translate("form_label_subtitle_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="text" maxlength="255" class="form-control" name="subtitle" placeholder="<?= translate('form_ph_subtitle_lang'); ?>" value="<?= $workus_object->subtitle; ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><?= translate("form_label_image_lang"); ?> (600 X 300)</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                            <input id="image-upload" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" name="archivo" <?= ($workus_object->resource == null) ? 'required' : ''; ?>>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label><?= translate("form_label_description_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <textarea name="description" rows="5" class="form-control" placeholder="<?= translate('form_ph_description_lang'); ?>" required><?= $workus_object->description; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-lg-offset-4">
                                        <?php if (strlen($workus_object->resource) > 0) { ?>
                                            <img style="width: 80%; margin-top: 25px;" class="img img-rounded img-responsive" src="<?= base_url($workus_object->resource); ?>" />
                                        <?php } ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: left;">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                                    </div>
                                </div>
                            </div>
                            <?= form_close(); ?>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(function() {

    });
</script>