<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('logo_config_lang'); ?>
            <small><?= translate('new_logo_lang'); ?></small>
            | <a href="<?= site_url('logo/index'); ?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> <?= translate('back_lang'); ?>
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('new_logo_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('new_logo_lang'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart("logo/add"); ?>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label><?= translate("logo_text_label_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="text" maxlength="255" class="form-control" name="text" placeholder="<?= translate('logo_text_ph_lang'); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><?= translate("image_label_lang"); ?> (<?= $img_width . ' x ' . $img_height; ?>)</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                            <input id="image-upload" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" required name="archivo">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><?= translate("section_table_title_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-align-justify"></i></span>
                                            <select name="section" class="form-control">
                                                <option value="header" selected>Header</option>
                                                <option value="shrinking-navbar">Navbar</option>
                                                <option value="footer">Footer</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: left;">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                                    </div>
                                </div>
                            </div>
                            <?= form_close(); ?>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(function() {

    });
</script>