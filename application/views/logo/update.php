<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('logo_config_lang'); ?>
            <small><?= translate('edit_logo_lang'); ?></small>
            | <a href="<?= site_url('logo/index'); ?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> <?= translate('back_lang'); ?>
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('edit_logo_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('edit_logo_lang'); ?></h3>
                    </div>
                    <div class="box-body">

                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart("logo/update"); ?>

                        <input type="hidden" value="active" name="is_active">
                        <input id="id-banner" type="hidden" name="logo_id" value="<?= $logo_object->_id; ?>">
                        <input id="old-file" type="hidden" name="old-file" value="<?= $logo_object->resource; ?>">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label><?= translate("logo_text_label_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="text" maxlength="255" class="form-control" name="text" placeholder="<?= translate('logo_text_ph_lang'); ?>" value="<?= $logo_object->text; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><?= translate("image_label_lang"); ?> (1300X721)</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                            <input id="image-upload" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" name="archivo">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><?= translate("section_table_title_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-align-justify"></i></span>
                                            <select name="section" class="form-control">
                                                <option value="header" <?= ($logo_object->section === "header") ? 'selected' : ''; ?>>Header</option>
                                                <option value="shrinking-navbar" <?= ($logo_object->section === "shrinking-navbar") ? 'selected' : ''; ?>>Navbar</option>
                                                <option value="footer" <?= ($logo_object->section === "footer") ? 'selected' : ''; ?>>Footer</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 col-lg-offset-3">
                                        <?php if (strlen($logo_object->resource) > 0) { ?>
                                            <img style="width: 50%;" class="img img-rounded img-responsive" src="<?= base_url($logo_object->resource); ?>" />
                                        <?php } ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: left;">
                                        <button id="btn-submit-form" type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <?= form_close(); ?>
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">

</script>