<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('our_clients_lang'); ?>
            <small><?= translate('edit_our_clients_lang'); ?></small>
            | <a href="<?= site_url('ourclient/index_ourclient'); ?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> <?= translate('back_lang'); ?>
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('edit_our_clients_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('edit_our_clients_lang'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart("ourclient/update_ourclient"); ?>

                        <input type="hidden" value="active" name="is_active">
                        <input id="our-cli-id" type="hidden" name="our-cli-id" value="<?= $ourclient_object->_id; ?>">
                        <input id="old-file" type="hidden" name="old-file" value="<?= $ourclient_object->resource; ?>">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label><?= translate("our_clients_name_label_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="text" maxlength="255" class="form-control" name="name" placeholder="<?= translate('our_clients_name_ph_lang'); ?>" value="<?= $ourclient_object->name; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><?= translate("our_clients_comment_label_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <textarea name="comment" rows="5" class="form-control" placeholder="<?= translate('our_clients_comment_ph_lang'); ?>" required><?= $ourclient_object->comment; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><?= translate("image_label_lang"); ?> (200 x 100)</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                            <input id="image-upload" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" name="archivo">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 col-lg-offset-8">
                                        <?php if (strlen($ourclient_object->resource) > 0) { ?>
                                            <img style="width: 50%;" class="img img-rounded img-responsive" src="<?= base_url($ourclient_object->resource); ?>" />
                                        <?php } ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: left;">
                                        <button id="btn-submit-form" type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <?= form_close(); ?>
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">

</script>