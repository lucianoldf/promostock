<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('our_clients_lang'); ?>
            <small><?= translate('new_our_clients_lang'); ?></small>
            | <a href="<?= site_url('ourclient/index_ourclient'); ?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> <?= translate('back_lang'); ?>
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('new_our_clients_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('new_our_clients_lang'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart("ourclient/add_ourclient"); ?>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label><?= translate("our_clients_name_label_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="text" maxlength="255" class="form-control" name="name" placeholder="<?= translate('our_clients_name_ph_lang'); ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><?= translate("our_clients_comment_label_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <textarea name="comment" rows="5" class="form-control" placeholder="<?= translate('our_clients_comment_ph_lang'); ?>" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><?= translate("image_label_lang"); ?> (200 x 100)</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                            <input id="image-upload" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" required name="archivo">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: left;">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                                    </div>
                                </div>
                            </div>
                            <?= form_close(); ?>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(function() {

    });
</script>