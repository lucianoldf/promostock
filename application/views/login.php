<!doctype html>
<html lang="en">

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= base_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url(); ?>assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Animation CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/templates/atz_shop/css/animate.css">
    <!-- Animation CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/templates/atz_shop/css/animate.css" type="text/css">
    <!-- Font Css -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="<?= base_url(); ?>/assets/templates/atz_shop/css/ionicons.min.css" type="text/css" rel="stylesheet">
    <!-- Owl Css -->
    <link href="<?= base_url(); ?>/assets/templates/atz_shop/css/owl.carousel.min.css" type="text/css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/templates/atz_shop/css/owl.theme.default.min.css" type="text/css" rel="stylesheet">
    <!-- Magnific Popup Css -->
    <link href="<?= base_url(); ?>/assets/templates/atz_shop/css/magnific-popup.css" type="text/css" rel="stylesheet">
    <!-- Bootstrap Css -->
    <link href="<?= base_url(); ?>/assets/templates/atz_shop/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <!-- Price Filter Css -->
    <link href="<?= base_url(); ?>/assets/templates/atz_shop/css/jquery-ui.css" type="text/css" rel="stylesheet">
    <!-- Scrollbar Css -->
    <link href="<?= base_url(); ?>/assets/templates/atz_shop/css/mCustomScrollbar.min.css" type="text/css" rel="stylesheet">
    <!-- Select2 Css -->
    <link href="<?= base_url(); ?>/assets/templates/atz_shop/css/select2.min.css" type="text/css" rel="stylesheet">
    <!-- main css -->
    <link href="<?= base_url(); ?>/assets/templates/atz_shop/css/style.css" type="text/css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/templates/atz_shop/css/responsive.css" type="text/css" rel="stylesheet">

    <!-- promostock custom -->
    <link href="<?= base_url(); ?>/assets/css/promostock.css" type="text/css" rel="stylesheet">
    <title>PromoStock</title>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106310707-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-106310707-1', { 'anonymize_ip': true });
</script> -->

    <!-- Start of StatCounter Code -->
    <script>
        // 	var sc_project=11921154;
        // 	var sc_security="6c07f98b";
        // 		var scJsHost = (("https:" == document.location.protocol) ?
        // 		"https://secure." : "http://www.");
        // document.write("<sc"+"ript src='" +scJsHost +"statcounter.com/counter/counter.js'></"+"script>");
    </script>
    <!-- End of StatCounter Code -->

</head>

<body class="hold-transition login-page" style="background-color:#fff;">
    <div id="" class="white-popup lr-popup text-center">
        <div>
            <a href="<?= site_url(); ?>"><img style="max-width:300px;" src="<?= base_url('assets/logos/logo.png'); ?>" /></a>
        </div><!-- /.login-logo -->
        <?= get_message_from_operation(); ?>
        <form class="subscribe-popup-form" method="post" action="login/auth">
        <br>
            <h4 class="text-center">Entrar</h4>
            <div id="alert-error-msg" class="alert alert-error" style="display: none"></div>
            <form id="form-login" class="subscribe-popup-form" method="post" action="#">
                <input name="email" required type="email" placeholder="Ingrese su Email">
                <input name="password" required type="password" placeholder="Ingrese su Contraseña">
                <div class="form-check text-left">
                    <!-- <label>Remember me
                    <input class="defult-check" type="checkbox" checked="checked">
                    <span class="checkmark"></span>
                </label> -->
                    <a href="#" class="forgot-password float-right">Olvido su Contraseña ?</a>
                </div>
                <button onclick="login();" class="btn btn-primary" title="Login" type="button">Entrar</button>
            </form>
            <h6>No tiene una cuenta ?</h6>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="forgotPwdModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <?= form_open("login/recover_password") ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Recuper contraseña</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Correo electrónico</label>
                                <input type="email" class="form-control" name="email" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-default" style="background-color:#006c7d !important;color:white !important">Recuperar contraseña</button>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>

    <!-- jQuery 2.1.4 -->
    <script src="<?= base_url(); ?>assets/templates/admin_lte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?= base_url(); ?>assets/templates/admin_lte/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?= base_url(); ?>assets/templates/admin_lte/plugins/iCheck/icheck.min.js"></script>

    <script type="text/javascript">
        function showModal() {
            $("#forgotPwdModal").modal('show');
        }
    </script>

</body>

</html>