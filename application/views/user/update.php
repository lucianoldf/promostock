<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('user_config_lang'); ?>
            <small><?= translate('edit_user_lang'); ?></small>
            | <a href="<?= site_url('user/index'); ?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> <?= translate('back_lang'); ?>
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('user_list_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('edit_user_lang'); ?></h3>
                    </div>
                    <div class="box-body">

                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart("user/update"); ?>
                        <div class="row">
                            <div class="col-lg-6">
                                <label><?= translate("name_label_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" class="form-control input-sm" name="fullname" required maxlength="255" value="<?= $user_object->name; ?>" placeholder="<?= translate('name_ph_lang'); ?>">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <label><?= translate("email_label_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon">@</span>
                                    <input type="email" class="form-control input-sm" required name="email" maxlength="255" value="<?= $user_object->email; ?>" readonly placeholder="<?= translate('email_ph_lang') ?>">
                                </div>
                                <br>
                                <div style="">
                                    <label><?= translate("role_label_lang"); ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <select class="form-control" name="role" id="role">    
                                            <?php
                                            if (isset($all_roles))
                                                foreach ($all_roles as $item) { ?>
                                                <option <?=($user_object->role_id == $item->_id)?'selected': null ?> value="<?= $item->_id; ?>"><?= $item->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <br>
                                </div>

                                <input type="hidden" name="user_id" value="<?= $user_object->_id; ?>" 
                                />
                                <input type="hidden" name="email_old" value="<?= $user_object->email; ?>"/> 
                            </div>
                        </div>

                        <div class="row" style="margin-top:15px;">
                            <div class="col-xs-12" style="text-align: left;">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                            </div>
                        </div>
                        <?= form_close(); ?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    $(function() {
        
    });
</script>