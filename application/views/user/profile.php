<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?= translate('edit_profile_lang'); ?> | <?= $user_object->name ?> | <a href="<?= site_url('user/index'); ?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> <?= translate('back_lang'); ?> </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('edit_profile_lang'); ?></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?= translate('edit_profile_lang'); ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart("user/edit_profile"); ?>
                        <input type="hidden" name="user_id" value="<?= $user_object->_id; ?>" />
                        <input type="hidden" name="email_old" value="<?= $user_object->email; ?>" />

                        <div class="row">
                            <div class="col-lg-6">
                                <label><?= translate("name_label_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" class="form-control input-sm" name="fullname" required value="<?= $user_object->name; ?>" placeholder="<?= translate('name_label_lang'); ?>">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label><?= translate("email_label_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon">@</span>
                                    <input type="email" class="form-control input-sm" required name="email" value="<?= $user_object->email; ?>" readonly placeholder="<?= translate('email_label_lang') ?>">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <label><?= translate("phone_label_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                                    <input type="text" maxlength="255" class="form-control input-sm" name="phone" value="<?= $user_object->phone; ?>" placeholder="<?= translate('phone_label_lang'); ?>">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label"><?= translate('photo_label_lang'); ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-file-image-o"></i></span>
                                        <input id="image-upload" accept=".jpg,.jpeg,.png,.bmp,.gif" type="file" name="archivo" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6">
                                <?php if(strlen($user_object->photo) > 0){ ?>
                                    <img style="width: 50%;" class="img img-rounded img-responsive" src="<?= base_url($user_object->photo); ?>" />
                                <?php } ?>
                            </div>
                        </div>

                        <div class="row" style="margin-top:15px;">
                            <div class="col-xs-12" style="text-align: left;">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                            </div>
                        </div>
                        <?= form_close(); ?>
                    </div><!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">

</script>