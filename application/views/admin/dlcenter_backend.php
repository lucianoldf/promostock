<!-- 
	Modal de confirmacion al eliminar elemento de un CRUD
-->
<div id="modal-delete" class="modal modal-danger fade" tabindex="-1" role="dialog" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?= translate('title_alert_user_delete'); ?></h4>
			</div>
			<div class="modal-body">
				<p><?= translate('message_alert_user_delete'); ?></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline pull-left" data-dismiss="modal"><?= translate('cancel'); ?></button>
				<button id="btn-confirm" type="button" class="btn bg-olive" onclick="" ;><?= translate('confirm'); ?></button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- ##################################################################### -->

<!-- 
	Div para mostaer mensajes de alerta en validaciones de formularios.
	Es insertado dinamicamente en la parte superior del contenedor "box-body"
	en los formularios para adicionar y actualizar datos
-->
<div id="alert-message" class="alert alert-danger alert-dismissable" style="display: none;">
	<h4><i class="icon fa fa-ban"></i> <?= translate('title_alert_message_lang'); ?></h4>
	<p></p>
</div>
<!-- ##################################################################### -->


<script type="text/javascript">
// TODO: Separar un un js
	/**
	 * Inicializamos variables y constantes utilizadas
	 */

	// Tipos de archivos de imagenes aceptados
	const FILETYPES_IMAGES = [
		'image/jpeg',
		'image/pjpeg',
		'image/png',
		'image/bmp',
		'image/gif'
	];

	// Tipos de archivos para importar aceptados
	const FILETYPES_IMPORT = [
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' // .xlsx
	];

	// Tipos de archivos comprimidos para importar aceptados
	const FILETYPES_IMPORT_ZIP = [
		'application/x-zip-compressed' // .xlsx
	];


	/**
	 * Los valores de las claves de estas constantes 
	 */
	const MESSAGES = [];
	const MAX_FILE_SIZE = [];

	// Para convertir MB a Byte
	const MB_TO_BYTE = 1048576;

	// Milisegundos de demora en ocultar mensajes mostrados al usuario
	const MS_HIDE_MESSAGE = 3000;

	MESSAGES['file_images_not_accept'] = '<?= ((translate('message_file_tipe_not_accept') != '')) ? translate('message_file_tipe_not_accept') : 'Tipo de archi no aceptado'; ?>';
	MESSAGES['file_import_not_accept'] = '<?= ((translate('message_file_tipe_not_accept_import') != '')) ? translate('message_file_tipe_not_accept_import') : 'Tipo de archi no aceptado'; ?>';
	MESSAGES['file_import_zip_not_accept'] = '<?= ((translate('message_file_tipe_not_accept_import_zip') != '')) ? translate('message_file_tipe_not_accept_import_zip') : 'Tipo de archi no aceptado'; ?>';

	MESSAGES['file_size_exceeded'] = '<?= ((translate('message_file_size_exceeded') != '')) ? translate('message_file_size_exceeded') : 'El archivo supera el limite maximo permitido'; ?>';
	MESSAGES['file_import_size_exceeded'] = '<?= ((translate('message_file_size_exceeded_import') != '')) ? translate('message_file_size_exceeded_import') : 'El archivo supera el limite maximo permitido'; ?>';

	MAX_FILE_SIZE['max_size_images'] = parseInt('<?= (null !== MAX_FILE_IMAGE_UPLOAD) ? MAX_FILE_IMAGE_UPLOAD : 5; ?>') * MB_TO_BYTE; //En bytes
	MAX_FILE_SIZE['max_size_import'] = parseInt('<?= (null !== MAX_FILE_IMPORT_UPLOAD) ? MAX_FILE_IMPORT_UPLOAD : 5; ?>') * MB_TO_BYTE; //En bytes

	let input_image_upload = null;
	let alert_message = null;
	let modal_delete = null;

	/**
	 * Document ready
	 */
	$(document).ready(function() {
		console.debug('Init validation admin backend');

		initModalDelete();

		insertDivAlertMessage();

		initFileTypeValidate();

	});



	function initModalDelete() {

		modal_delete = $('#modal-delete');

		modal_delete.on('shown.bs.modal	', function(e) {

			//get data-id attribute of the clicked element
			let id = $(e.relatedTarget).data('id');
			let url = $(e.relatedTarget).data('action') + '/' + id;

			// console.log('mostrar modal con id: ' + id + ' - url: ' + url);
			$("#btn-confirm").attr('onclick', "window.location.href = '" + url + "'");
		});
	}


	function insertDivAlertMessage() {
		$(".box-body").prepend($("#alert-message"));
	}


	function initFileTypeValidate() {

		input_image_upload = $("input[id*='image-upload']");
		input_import_upload = $("input[id*='import-upload']");
		input_import_zip_upload = $("input[id*='import-zip-upload']");
		alert_message = $("#alert-message");

		/**
		 * Validate img upload
		 */
		input_image_upload.each(function() {
			$(this).on("change", (e) => {

				if (window.File && window.FileReader && window.FileList && window.Blob) {

					//get the file size and file type from file input field
					let fileUpload = e.target.files[0];

					// Validate type file
					if (!validFileType(fileUpload, FILETYPES_IMAGES)) {
						// console.log("Tipo de archivo Incorrecto");
						showMessage(MESSAGES['file_not_accept']);
						resetValues($(this));
						$('html, body').animate({
							scrollTop: alert_message.position().top
						}, 'slow');
					}

					// Valid max file size
					if (fileUpload.size > MAX_FILE_SIZE['max_size_images']) {
						// console.log('Supera los 5 mb');
						showMessage(MESSAGES['file_size_exceeded']);
						resetValues($(this));
						$('html, body').animate({
							scrollTop: alert_message.position().top
						}, 'slow');
					}
				}
			});
		});


		/**
		 * Validate import spreadsheet upload
		 */
		input_import_upload.each(function() {
			$(this).on("change", (e) => {

				if (window.File && window.FileReader && window.FileList && window.Blob) {

					//get the file size and file type from file input field
					let fileUpload = e.target.files[0];

					// Validate type file
					if (!validFileType(fileUpload, FILETYPES_IMPORT)) {
						// console.log("Tipo de archivo Incorrecto");
						showMessage(MESSAGES['file_import_not_accept']);
						resetValues($(this));
						$('html, body').animate({
							scrollTop: alert_message.position().top
						}, 'slow');
					}

					// Valid max file size
					if (fileUpload.size > MAX_FILE_SIZE['max_size_import']) {
						// console.log('Supera los 5 mb');
						showMessage(MESSAGES['file_import_size_exceeded']);
						resetValues($(this));
						$('html, body').animate({
							scrollTop: alert_message.position().top
						}, 'slow');
					}
				}
			});
		});


		/**
		* Validate import zip upload
		*/
		input_import_zip_upload.each(function() {
			$(this).on("change", (e) => {

				if (window.File && window.FileReader && window.FileList && window.Blob) {

					//get the file size and file type from file input field
					let fileUpload = e.target.files[0];

					// Validate type file
					if (!validFileType(fileUpload, FILETYPES_IMPORT_ZIP)) {
						// console.log("Tipo de archivo Incorrecto");
						showMessage(MESSAGES['file_import_zip_not_accept']);
						resetValues($(this));
						$('html, body').animate({
							scrollTop: alert_message.position().top
						}, 'slow');
					}

					// Valid max file size
					if (fileUpload.size > MAX_FILE_SIZE['max_size_import']) {
						// console.log('Supera los 5 mb');
						showMessage(MESSAGES['file_import_size_exceeded']);
						resetValues($(this));
						$('html, body').animate({
							scrollTop: alert_message.position().top
						}, 'slow');
					}
				}
			});
		});
	}



	function resetValues(input) {
		input.val('');
	}

	function validFileType(file, file_type) {
		for (let i = 0; i < file_type.length; i++) {
			if (file.type === file_type[i]) {
				return true;
			}
		}
		return false;
	}

	function showMessage(message) {
		alert_message.find('p').html(message);
		alert_message.show(1);
		setTimeout(() => {
			alert_message.fadeOut(2000)
		}, MS_HIDE_MESSAGE);
	}
</script>