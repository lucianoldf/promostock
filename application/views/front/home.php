<?= get_message_from_operation(); ?>

<!-- <div id="alert-error-msg" class="alert alert-error" style="display: none"></div> -->

<!-- Start Slider Section -->
<section class="slider slider-type-2 p-0 mx-15">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 slider-width">
				<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<?php if ($banners) {
							$firs_flag = TRUE; ?>
							<?php foreach ($banners as $banner) { ?>
								<div class="carousel-item <?= ($firs_flag) ? 'active' : '' ?> slider-area background_bg" data-img-src="<?= base_url($banner->resource); ?>">
									<div class="banner_slide_content">
										<div class="slider-caption <?= $banner->align_text; ?>">
											<h1 style="color: <?= $banner->color_text; ?>" class="animation" data-animation="fadeInDown" data-animation-delay="0.2s"><?= $banner->title; ?></h1>
											<p style="color: <?= $banner->color_text; ?>" class="animation" data-animation="fadeInUp" data-animation-delay="0.6s"><?= $banner->subtitle; ?></p>
											<?php if (strlen(trim($banner->btn_link)) > 0) { ?>
												<a href="<?= $banner->btn_link; ?>" target="<?= ($banner->btn_link_blank) ? '_blank' : '_self'; ?>" class="btn btn-primary animation" data-animation="fadeInUp" data-animation-delay="1s"><?= $banner->btn_name; ?></a>
											<?php } ?>
										</div>
									</div>
								</div>
							<?php
								$firs_flag = FALSE;
							} ?>
						<?php } else { ?>
							<div class="carousel-item active slider-area background_bg" data-img-src="<?= base_url(); ?>/assets/images/slider-example/slide-1.jpg">
								<div class="banner_slide_content">
									<div class="slider-caption">
										<h1 class="animation" data-animation="fadeInDown" data-animation-delay="0.2s">Venta de Verano!</h1>
										<p class="animation" data-animation="fadeInUp" data-animation-delay="0.6s">¡Obtenga hasta un 50% de descuento en sus artículos de compras mañana!</p>
										<a href="shop.html" class="btn btn-primary animation" data-animation="fadeInUp" data-animation-delay="1s">Comprar Ahora</a>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<ol class="carousel-indicators">
						<?php if ($banners) {
							$idx = 0; ?>
							<?php foreach ($banners as $banner) { ?>
								<li data-target="#carouselExampleControls" data-slide-to="<?= $idx; ?>" class="<?= ($idx === 0) ? 'active' : '' ?>"></li>
								<?php $idx++; ?>
							<?php } ?>
						<?php } else { ?>
							<li data-target="#carouselExampleControls" data-slide-to="0" class="active"></li>
						<?php } ?>
					</ol>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Slider Section -->

<!-- Start Newsletter Section -->
<section class="pt_medium pb_medium navy-dark-bg m-0 px-0">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-6">
				<div class="text-white mb-3 mb-md-0">
					<h5 class="mb-2">Sigue nuestras actualizaciones!</h5>
					<p>Si desea recibir un correo electrónico de nosotros cada vez que tengamos una nueva oferta especial, regístrese aquí!</p>
				</div>
			</div>
			<div class="col-md-6">
				<form class="newsletter-form" method="post" action="<?= site_url('home/suscriber'); ?>">
					<div class="outline-input">
						<input type="email" required="" name="email" placeholder="Ingrese su direccion de Email">
					</div>
					<button type="submit" title="Suscribete" class="btn btn-submit border-0 btn-primary" name="submit" value="Submit">Enviar</button>
				</form>
			</div>
		</div>
	</div>
</section>
<!-- End Newsletter Section -->

<!-- Start Service Section -->
<section class="bg-white section-shadow">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-6 facility-box box-1">
				<div class="facility-inner">
					<div class="fb-icon">
						<!-- <i class="fa fa-truck"></i> -->
						<img src="<?= base_url(); ?>/assets/images/001-recommended.png" alt="">
					</div>
					<div class="fb-text">
						<h5>CALIDAD</h5>
						<span>Los mejores promocionales</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-6 facility-box box-2">
				<div class="facility-inner">
					<div class="fb-icon">
						<!-- <i class="fa fa-headphones"></i> -->
						<img src="<?= base_url(); ?>/assets/images/002-call-center.png" alt="">
					</div>
					<div class="fb-text">
						<h5>SOLUCIONES</h5>
						<span>Más que promocionales</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-6 facility-box box-3">
				<div class="facility-inner">
					<div class="fb-icon">
						<!-- <i class="fa fa-cc-mastercard"></i> -->
						<img src="<?= base_url(); ?>/assets/images/003-shield.png" alt="">
					</div>
					<div class="fb-text">
						<h5>SOPORTE</h5>
						<span>Asesoría personalizada</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-6 facility-box box-4">
				<div class="facility-inner">
					<div class="fb-icon">
						<!-- <i class="fa fa-trophy"></i> -->
						<img src="<?= base_url(); ?>/assets/images/004-scoring.png" alt="">
					</div>
					<div class="fb-text">
						<h5>SATISFACCIÓN</h5>
						<span>100% garantizada</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Service Section -->



<!-- Start Deal Section -->
<section class="deals-section section-shadow bg-white">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="title">
					<h4><?= translate('title_deal_section'); ?></h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-9 col-md-8 col-sm-7">
				<div class="products-slider4 products-style-2 nav-style-2 owl-carousel owl-theme" data-margin="30" data-dots="false" data-autoplay="false" data-nav="true" data-loop="false">
					<?php foreach ($products_deal as $product) { ?>
						<div class="item">
							<div class="product-box common-cart-box">
								<div class="product-img common-cart-img">
									<img src="<?= base_url($product->resource_catalog); ?>" alt="product-img">
									<div class="hover-option">
										<!-- <div class="add-cart-btn">
										<a href="#" class="btn btn-primary">Add To Cart</a>
									</div> -->
										<ul class="hover-icon">
											<li><a href="#"><i class="fa fa-cart-plus" aria-hidden="true"></i></a></li>
											<!-- <li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a></li> -->
											<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
											<!-- <li><a href="#"><i class="fas fa-refresh"></i></a></li> -->
										</ul>
										<!-- <ul class="product-color">
										<li><a href="#" class="blue-dot"></a></li>
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
									</ul> -->
									</div>
								</div>
								<div class="product-info common-cart-info">
									<a href="<?= site_url('product_detail/' . $product->_id); ?>" class="cart-name"><?= $product->name; ?></a>
									<div class="product-rate">
										<?php
										$starts_average = round($product->starts_average, 1, PHP_ROUND_HALF_ODD);
										for ($i = 1; $i <= 5; $i++) {
											if ($i < $starts_average) {
												echo '<i class="ion-android-star"></i>';
											} else if ($i / 2 == $starts_average) {
												echo '<i class="ion-android-star-half"></i>';
											} else if ($i == $starts_average) {
												echo '<i class="ion-android-star"></i>';
												break;
											} else if ($starts_average == 0) {
												echo '<br>';
												break;
											}
										}
										?>
									</div>
									<!-- <p class="cart-price"><del>10</del>5</p> -->
									<p class="cart-price"><?= SIMBOL_MONEY . " " . number_format($product->price_final, 2); ?></p>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-5">
				<div class="carousel_slide1 shop-hover-style-2 owl-carousel owl-theme mt-3 mt-sm-0" data-dots="false">

					<?php foreach ($products_deal_banner as $product) { ?>
						<a href="#" class="offer-banner shop-hover">
							<img src="<?= base_url($product->resource_main); ?>" alt="offer-banner">
							<div class="shop-hover-text">
								<div class="shop-hover-wrap">
									<h5><?= $product->name; ?></h5>
								</div>
							</div>
						</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Deal Section -->



<!-- Start Catalog Section -->
<br><br>
<section id="catalogos" class="offer-section shop-hover-style-2 section-shadow bg-white">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="title">
					<h4>Catálogos de temporada</h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<a href="<?= base_url('uploads/tmp-pdf/TEMP-009-Cátalogo Artículos Promocionales Navidad.pdf'); ?>" target="_blank" class="offer-banner shop-hover">
					<img src="<?= base_url(); ?>/assets/images/banner-1.png" alt="offer-banner">
					<div class="shop-hover-text">
						<div class="shop-hover-wrap">
							<h5>Promocionales de Navidad</h5>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?= base_url('uploads/tmp-pdf/TEMP-010-Catálogo-Regalos Corporativos Navidad.pdf'); ?>" target="_blank" class="offer-banner shop-hover">
					<img src="<?= base_url(); ?>/assets/images/banner-2.png" alt="offer-banner">
					<div class="shop-hover-text">
						<div class="shop-hover-wrap">
							<h5>Regalos de Navidad</h5>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?= base_url('uploads/tmp-pdf/TEMP-010-Catálogo-Regalos Corporativos Navidad.pdf'); ?>" target="_blank" class="offer-banner shop-hover">
					<img src="<?= base_url(); ?>/assets/images/banner-2.png" alt="offer-banner">
					<div class="shop-hover-text">
						<div class="shop-hover-wrap">
							<h5>Temporada Playera</h5>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
<!-- End Catalog Section -->


<!-- Start Popular Products Section -->
<section class="products-section section-shadow bg-white">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="title">
					<h4><?= translate('title_new_section'); ?></h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="carousel_slide6 products-style-2 nav-style-2 owl-carousel owl-theme" data-margin="30" data-dots="false" data-autoplay="false" data-nav="true" data-loop="false">
					<?php foreach ($products_new as $product) { ?>
						<div class="item">
							<div class="product-box common-cart-box">
								<div class="product-img common-cart-img">
									<img src="<?= base_url($product->resource_catalog); ?>" alt="product-img">
									<div class="hover-option">
										<!-- <div class="add-cart-btn">
										<a href="#" class="btn btn-primary">Add To Cart</a>
									</div> -->
										<ul class="hover-icon">
											<li><a href="#"><i class="fa fa-cart-plus" aria-hidden="true"></i></a></li>
											<!-- <li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a></li> -->
											<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
											<!-- <li><a href="#"><i class="fas fa-refresh"></i></a></li> -->
										</ul>
										<!-- <ul class="product-color">
										<li><a href="#" class="blue-dot"></a></li>
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
									</ul> -->
									</div>
								</div>
								<div class="product-info common-cart-info">
									<a href="product-detail.html" class="cart-name"><?= $product->name; ?></a>
									<div class="product-rate">
										<?php
										$starts_average = round($product->starts_average, 1, PHP_ROUND_HALF_ODD);
										for ($i = 1; $i <= 5; $i++) {
											if ($i < $starts_average) {
												echo '<i class="ion-android-star"></i>';
											} else if ($i / 2 == $starts_average) {
												echo '<i class="ion-android-star-half"></i>';
											} else if ($i == $starts_average) {
												echo '<i class="ion-android-star"></i>';
												break;
											} else if ($starts_average == 0) {
												echo '<br>';
												break;
											}
										}
										?>
									</div>
									<!-- <p class="cart-price"><del>10</del>5</p> -->
									<p class="cart-price"><?= SIMBOL_MONEY . " " . number_format($product->price_final, 2); ?></p>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Popular Products Section -->



<section class="information-section" style="background-image: url(<?= base_url(); ?>/assets/images/bg-vision.jpg);">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<h3>Solicita mas información ahora</h3>
			</div>
			<div class="col-md-4">
				<a href="<?= site_url('contact_front'); ?>"><button type="submit">Más información</button></a>
			</div>
		</div>
	</div>
</section>



<!-- Start Popular Products Section -->
<section class="products-section section-shadow bg-white">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="title">
					<h4><?= translate('title_popular_section'); ?></h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="carousel_slide6 products-style-2 nav-style-2 owl-carousel owl-theme" data-margin="30" data-dots="false" data-autoplay="false" data-nav="true" data-loop="false">
					<?php foreach ($products_popular as $product) { ?>
						<div class="item">
							<div class="product-box common-cart-box">
								<div class="product-img common-cart-img">
									<img src="<?= base_url($product->resource_catalog); ?>" alt="product-img">
									<div class="hover-option">
										<!-- <div class="add-cart-btn">
										<a href="#" class="btn btn-primary">Add To Cart</a>
									</div> -->
										<ul class="hover-icon">
											<li><a href="#"><i class="fa fa-cart-plus" aria-hidden="true"></i></a></li>
											<!-- <li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a></li> -->
											<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
											<!-- <li><a href="#"><i class="fas fa-refresh"></i></a></li> -->
										</ul>
										<!-- <ul class="product-color">
										<li><a href="#" class="blue-dot"></a></li>
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
									</ul> -->
									</div>
								</div>
								<div class="product-info common-cart-info">
									<a href="product-detail.html" class="cart-name"><?= $product->name; ?></a>
									<div class="product-rate">
										<?php
										$starts_average = round($product->starts_average, 1, PHP_ROUND_HALF_ODD);
										for ($i = 1; $i <= 5; $i++) {
											if ($i < $starts_average) {
												echo '<i class="ion-android-star"></i>';
											} else if ($i / 2 == $starts_average) {
												echo '<i class="ion-android-star-half"></i>';
											} else if ($i == $starts_average) {
												echo '<i class="ion-android-star"></i>';
												break;
											} else if ($starts_average == 0) {
												echo '<br>';
												break;
											}
										}
										?>
									</div>
									<!-- <p class="cart-price"><del>10</del>5</p> -->
									<p class="cart-price"><?= SIMBOL_MONEY . " " . number_format($product->price_final, 2); ?></p>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Popular Products Section -->


<!-- Start Testimonial Section -->
<section class="testimonial-section section-shadow bg-white">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="testimonial_slider nav-style-2 owl-carousel owl-theme" data-margin="30" data-dots="true" data-autoplay="false" data-nav="false" data-loop="true">
					<?php if (isset($our_clients)) { ?>

						<?php foreach ($our_clients as $testimonial) { ?>

							<div class="item">
								<div class="fancy-box">
									<div class="fancy-img">
										<img src="<?= base_url($testimonial->resource); ?>" alt="testimonial-img">
									</div>
									<div class="fancy-wrap">
										<h5 class="testimonial-title"><a href="#"><?= $testimonial->name; ?></a>
											<small class="post">Web Developer</small>
										</h5>
										<p class="description"><?= $testimonial->comment; ?></p>
									</div>
								</div>
							</div>

						<?php } ?>

					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Testimonial Section -->


<!-- Start Best Saller Section -->
<section class="best-saller-section section-shadow bg-white">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="title">
					<h4><?= translate('title_most_view_section'); ?></h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="carousel_slide6 products-style-2 nav-style-2 owl-carousel owl-theme" data-margin="30" data-dots="false" data-autoplay="false" data-nav="true" data-loop="false">
					<?php foreach ($products_most_views as $product) { ?>
						<div class="item">
							<div class="product-box common-cart-box">
								<div class="product-img common-cart-img">
									<img src="<?= base_url($product->resource_catalog); ?>" alt="product-img">
									<div class="hover-option">
										<!-- <div class="add-cart-btn">
										<a href="#" class="btn btn-primary">Add To Cart</a>
									</div> -->
										<ul class="hover-icon">
											<li><a href="#"><i class="fa fa-cart-plus" aria-hidden="true"></i></a></li>
											<!-- <li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a></li> -->
											<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
											<!-- <li><a href="#"><i class="fas fa-refresh"></i></a></li> -->
										</ul>
										<!-- <ul class="product-color">
										<li><a href="#" class="blue-dot"></a></li>
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
									</ul> -->
									</div>
								</div>
								<div class="product-info common-cart-info">
									<a href="product-detail.html" class="cart-name"><?= $product->name; ?></a>
									<div class="product-rate">
										<?php
										$starts_average = round($product->starts_average, 1, PHP_ROUND_HALF_ODD);
										for ($i = 1; $i <= 5; $i++) {
											if ($i < $starts_average) {
												echo '<i class="ion-android-star"></i>';
											} else if ($i / 2 == $starts_average) {
												echo '<i class="ion-android-star-half"></i>';
											} else if ($i == $starts_average) {
												echo '<i class="ion-android-star"></i>';
												break;
											} else if ($starts_average == 0) {
												echo '<br>';
												break;
											}
										}
										?>
									</div>
									<!-- <p class="cart-price"><del>10</del>5</p> -->
									<p class="cart-price"><?= SIMBOL_MONEY . " " . number_format($product->price_final, 2); ?></p>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Best Saller Section -->


<!-- Start Offer Banner Section -->
<!-- <section class="offer-section shop-hover-style-2 section-shadow bg-white">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<a href="#" class="offer-banner shop-hover">
					<img src="<?= base_url(); ?>/templates/atz_shop/image/offer-banner-10.jpg" alt="offer-banner">
					<div class="shop-hover-text">
						<div class="shop-hover-wrap">
							<h5>Black Fashion</h5>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="#" class="offer-banner shop-hover">
					<img src="<?= base_url(); ?>/templates/atz_shop/image/offer-banner-11.jpg" alt="offer-banner">
					<div class="shop-hover-text">
						<div class="shop-hover-wrap">
							<h5>White Fashion</h5>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="#" class="offer-banner shop-hover">
					<img src="<?= base_url(); ?>/templates/atz_shop/image/offer-banner-12.jpg" alt="offer-banner">
					<div class="shop-hover-text">
						<div class="shop-hover-wrap">
							<h5>Classic Fashion</h5>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</section> -->
<!-- End Offer Banner Section -->


<!-- Start New Collection Section -->
<!-- <section class="new-collection-section section-shadow bg-white">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="title">
					<h4>New Collection</h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="carousel_slide6 products-style-2 nav-style-2 owl-carousel owl-theme" data-margin="30" data-dots="false" data-autoplay="false" data-nav="true" data-loop="false">
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-1.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="blue-dot"></a></li>
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Variable product 001</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 90.00</del>$ 78.00</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-2.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="white-dot"></a></li>
										<li><a href="#" class="yellow-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
										<li><a href="#" class="red-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Ornare sed consequat</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 95.00</del>$ 81.00</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-3.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
										<li><a href="#" class="orange-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Pellentesque posuere</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 90.00</del>$ 80.00</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-4.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="yellow-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Consequat posuere</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 50.00</del>$ 45.00</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-1.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="blue-dot"></a></li>
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Variable product 001</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 90.00</del>$ 78.00</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-2.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="white-dot"></a></li>
										<li><a href="#" class="yellow-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
										<li><a href="#" class="red-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Ornare sed consequat</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 95.00</del>$ 81.00</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-3.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
										<li><a href="#" class="orange-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Pellentesque posuere</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 90.00</del>$ 80.00</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-4.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="yellow-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Consequat posuere</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 50.00</del>$ 45.00</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-1.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="blue-dot"></a></li>
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Variable product 001</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 90.00</del>$ 78.00</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-2.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="white-dot"></a></li>
										<li><a href="#" class="yellow-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
										<li><a href="#" class="red-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Ornare sed consequat</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 95.00</del>$ 81.00</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-3.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
										<li><a href="#" class="orange-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Pellentesque posuere</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 90.00</del>$ 80.00</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-4.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="yellow-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Consequat posuere</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 50.00</del>$ 45.00</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-1.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="blue-dot"></a></li>
										<li><a href="#" class="black-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Variable product 001</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 90.00</del>$ 78.00</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="product-box common-cart-box">
							<div class="product-img common-cart-img">
								<img src="<?= base_url(); ?>/templates/atz_shop/image/product-img-2.jpg" alt="product-img">
								<div class="hover-option">
									<ul class="hover-icon">
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye"></i></a></li>
										<li><a href="#"><i class="fa fa-refresh"></i></a></li>
									</ul>
									<ul class="product-color">
										<li><a href="#" class="white-dot"></a></li>
										<li><a href="#" class="yellow-dot"></a></li>
										<li><a href="#" class="pink-dot"></a></li>
										<li><a href="#" class="red-dot"></a></li>
									</ul>
								</div>
							</div>
							<div class="product-info common-cart-info">
								<a href="product-detail.html" class="cart-name">Ornare sed consequat</a>
								<div class="product-rate">
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>
									<i class="ion-android-star"></i>

									<i class="ion-android-star-half"></i>
								</div>
								<p class="cart-price"><del>$ 95.00</del>$ 81.00</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->
<!-- End New Collection Section -->


<!-- Start Instagram Section -->
<!-- <section class="instagram-section p-0 mx-0 mb-0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 p-0">
				<div class="insta-slider owl-carousel owl-theme popup-gallery">
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-1.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-1.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-2.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-2.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-3.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-3.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-4.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-4.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-5.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-5.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-6.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-6.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-7.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-7.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-8.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-8.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-9.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-9.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-10.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-10.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-11.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-11.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-12.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-12.jpg"><i class="fa fa-instagram"></i></a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 p-0">
				<div class="insta-slider-2 owl-carousel owl-theme popup-gallery">
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-1.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-1.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-2.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-2.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-3.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-3.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-4.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-4.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-5.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-5.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-6.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-6.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-7.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-7.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-8.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-8.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-9.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-9.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-10.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-10.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-11.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-11.jpg"><i class="fa fa-instagram"></i></a>
					</div>
					<div class="insta-img">
						<img src="<?= base_url(); ?>/templates/atz_shop/image/insta-img-12.jpg" alt="insta-img"><a href="<?= base_url(); ?>/templates/atz_shop/image/instabig-img-12.jpg"><i class="fa fa-instagram"></i></a>
					</div>
				</div>
			</div>
		</div>
		<div class="follow-box">
			<i class="fa fa-instagram"></i>
			<h3>instagram</h3>
			<span>@atzshop</span>
			<a href="#" class="btn btn-primary">Follow</a>
		</div>
	</div>
</section> -->
<!-- End Instagram Section -->