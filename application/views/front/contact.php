<style>
.map-square {
	padding: 5px !important;
}
</style>
<?= get_message_from_operation(); ?>

<!-- Start Breadcrumbs Section -->
<section class="breadcrumbs-section background_bg" data-img-src="<?= base_url($contact->resource); ?>">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="page_title text-center">
					<h1>Contáctenos</h1>
					<ul class="breadcrumb justify-content-center">
						<li><a href="<?= site_url(); ?>">Inicio</a></li>
						<li><span>Contáctenos</span></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Header Section -->

<!-- Start Contact Detail Section -->
<section class="contact-inner-page pt_large">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-12">
				<div class="contact-inputs">
					<div class="title text-left">
						<h4><?= $contact->title; ?></h4>
					</div>
					<form method="post" class="contact-form form-with-label" action="<?= site_url('contact/send_email'); ?>">
						<input id="contact-lat" type="hidden" value="<?= $contact->location->latitude; ?>">
						<input id="contact-long" type="hidden" value="<?= $contact->location->longuitude; ?>">
						<input id="contact-lat-2" type="hidden" value="<?= $contact->location->latitude_2; ?>">
						<input id="contact-long-2" type="hidden" value="<?= $contact->location->longuitude_2; ?>">

						<div class="input-1">
							<label><?= translate('contact_label_name_lang'); ?><span>*</span></label>
							<input required class="form-control" name="first-name" id="first-name" placeholder="Ingrese Su Nombre..." value="" type="text">
						</div>
						<div class="input-2">
							<label><?= translate('contact_label_email_lang'); ?><span>*</span></label>
							<input required class="form-control" name="email" id="email" placeholder="Ingrese Su E-mail..." value="" type="email">
						</div>
						<div class="input-3">
							<label><?= translate('contact_label_phone_lang'); ?><span>*</span></label>
							<input required class="form-control" name="phone" id="phone" placeholder="Ingrese Su Teléfono..." value="" type="text">
						</div>
						<div class="input-4">
							<label><?= translate('contact_label_subject_lang'); ?><span>*</span></label>
							<input required="" class="form-control" name="subject" id="subject" placeholder="Ingrese Su Asunto..." value="" type="text">
						</div>
						<div class="input-5">
							<label><?= translate('contact_label_message_lang'); ?><span>*</span></label>
							<textarea required="" rows="3" class="form-control" name="description" id="description" placeholder="Ingrese un Mensaje..."></textarea>
						</div>
						<div class="submit-btn">
							<button type="submit" id="submitButton" class="btn btn-primary">Enviar</button>
						</div>
						<div class="col-md-12">
							<div id="alert-msg" class="alert-msg text-center"></div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-lg-4 col-md-12">
				<div class="contact-details">
					<div class="title text-left">
						<h4>CONTÁCTANOS</h4>
					</div>
					<div class="contact-inner">
						<p><?= $contact->text; ?></p>
						<ul class="contact-locations">
							<li><span class="fa fa-map-marker"></span><p><strong>Quito:</strong> <?= $contact->address; ?></p></li>
							<li><span class="fa fa-map-marker"></span><p><strong>Guayaquil:</strong> <?= 'Samborondón Office Center piso 2 oficina 229 km 1,5 vía Samborondón' ?></p></li>
							<li><span class="fa fa-phone"></span>
							<?php $html_phone = ''; $last = count($contact->phone) - 1;
							foreach($contact->phone as $idx => $phone){
								$html_phone .= '<a href="tel:'.$phone['tel'].'"><p>' . $phone['string'] . '</p></a>';
								if($idx < $last){
									$html_phone .= '<p>&nbsp;/&nbsp;</p>';
								}
							};
							$html_phone .= '</li>';
							echo $html_phone;
							?>
							<li><span class="fa fa-envelope-o"></span><a href="mailto:<?= $contact->email; ?>"><?= $contact->email; ?></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Contact Detail Section -->

<!-- Start Map Section -->
<!-- <section class="pb-0">
	<div class="container-fluid p-0">
		<div class="row no-gutters">
			<div class="col-md-12">
				<div class="map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.793577934378!2d-78.48369258556414!3d-0.2017819354606406!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91d59a0ca6b9b63d%3A0xc0a30f9a4c7216b1!2sPromostock%20SA!5e0!3m2!1ses-419!2sec!4v1573668336102!5m2!1ses-419!2sec"  width="1343" height="420" frameborder="0" style="border:0;" allowfullscreen=""></iframe>			
				</div>
			</div>
		</div>
	</div>
</section> -->


<!-- <div class="row">
	<div class="col-md-12">
		<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.793577934378!2d-78.48369258556414!3d-0.2017819354606406!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91d59a0ca6b9b63d%3A0xc0a30f9a4c7216b1!2sPromostock%20SA!5e0!3m2!1ses-419!2sec!4v1573668336102!5m2!1ses-419!2sec"  width="1343" height="420" frameborder="0" style="border:0;" allowfullscreen=""></iframe>			
		</div>
	</div>
</div> -->
	


<!-- Start Map Section -->
<section class="pb-0">
	<div class="container-fluid p-0">
		<div class="row no-gutters">
			<div class="map-square col-md-6">
				<div class="google-maps-wrapper">
					<div id="google-maps-inner" class="google-maps-inner">
					<div class="map" id="map"></div>
					</div>
				</div>
				
			</div>
			<div class="map-square col-md-6">
				<div class="google-maps-wrapper">
					<div id="google-maps-inner" class="google-maps-inner">
					<div class="map" id="map-2"></div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>
<!-- End Map Section -->

<script>

contact_lat = $("#contact-lat").val();
contact_long =  $("#contact-long").val();
contact_lat_2 = $("#contact-lat-2").val();
contact_long_2 =  $("#contact-long-2").val();
</script>