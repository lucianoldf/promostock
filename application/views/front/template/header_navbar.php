<!-- Start Header Section -->
<header class="header-3">
	<div class="header-top bg-white">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-lg-2 col-sm-3">
					<div class="header-logo">
						<a href="<?= site_url(); ?>">
							<!-- <object type="image/svg+xml" data="<?= base_url(); ?>/assets/logos/logo-promostock.svg">
							
							</object> -->
							<!-- <img src="<?= base_url(); ?>/assets/logos/logo-promostock.svg" onerror="this.onerror=null; this.src='<?= base_url(); ?>/assets/logos/logo.png'"> -->
							<img src="<?= base_url($this->session->userdata('logos')['header']->resource);?>" alt="logo">
						</a>
					</div>
				</div>
				<div class="col-lg-5 col-sm-9 order-lg-last">
					<ul class="header-options">
						<?php if($this->config->item('is_show_login') === TRUE) { ?>

							<li><i class="fa fa-sign-in" aria-hidden="true"></i> <a href="#test-popup1" class="open-popup-link">Entrar & Registrarse</a>
								<div id="test-popup1" class="white-popup lr-popup mfp-hide text-center">
									<h4>Entrar</h4>
									<div id="alert-error-msg" class="alert alert-error" style="display: none"></div>
									<form id="form-login" class="subscribe-popup-form" method="post" action="#">
										<input name="email" required type="email" placeholder="Ingrese su Email">
										<input name="password" required type="password" placeholder="Ingrese su Contraseña">
										<div class="form-check text-left">
											<!-- <label>Remember me
												<input class="defult-check" type="checkbox" checked="checked">
												<span class="checkmark"></span>
											</label> -->
											<a href="#" class="forgot-password float-right">Olvido su Contraseña ?</a>
										</div>
										<button onclick="login();" class="btn btn-primary" title="Login" type="button">Entrar</button>
									</form>
									<h6>No tiene una cuenta ?</h6>
									<a href="#test-popup2" class="sign-up open-popup-link">Click aqui para registrarse</a>
								</div>
								<div id="test-popup2" class="white-popup lr-popup mfp-hide">
									<h4>Registrarse</h4>
									<form class="subscribe-popup-form" method="post" action="#">
										<input name="input" required type="input" placeholder="Enter Your name">
										<input name="email" required type="email" placeholder="Enter Your Email">
										<input name="password" required type="password" placeholder="Enter Your Password">
										<input name="password" required type="password" placeholder="Confirmation Password">
										<div class="form-check">
											<label>I accept the terms and conditions
												<input class="defult-check" type="checkbox" checked="checked">
												<span class="checkmark"></span>
											</label>
										</div>
										<button class="btn btn-primary" title="Subscribe" type="button">Register</button>
									</form>
								</div>
							</li>
						<?php } ?>
						<!-- <li><a href="#">My Account</a></li> -->
						<!-- <li><a href="#">Wishlist</a></li> -->
						<!-- <li class="header-cart">
							<a href="#">
								<div class="cart-icon">
								<i class="ion-ios-cart"></i>
									<span>02</span>
								</div>Cart
								<i class="fa fa-angle-down"></i>
							</a>
							<div class="cart-box cart_box_left">
								<div class="cart-info">
									<div class="cart-prodect d-flex justify-content-between">
										<div class="cart-img">
											<img src="image/cart-img-1.png" alt="cart-img">
										</div>
										<div class="cart-product">
											<a href="#">Ornare sed consequat</a>
											<p>$ 81.00</p>
										</div>
										<a href="#" class="close-icon d-flex align-items-center"><i class="ion-close"></i></a>
									</div>
									<div class="cart-prodect d-flex justify-content-between">
										<div class="cart-img">
											<img src="image/cart-img-2.png" alt="cart-img">
										</div>
										<div class="cart-product">
											<a href="#">Pellentesque posuere</a>
											<p>$ 80.00</p>
										</div>
										<a href="#" class="close-icon d-flex align-items-center"><i class="ion-close"></i></a>
									</div>
								</div>
								<div class="price-prodect d-flex align-items-center justify-content-between">
									<p class="total">total</p>
									<p class="total-price">$ 161.00</p>
								</div>
								<div class="cart-btn">
									<a href="cart.html" class="btn btn-primary">View Cart</a>
								</div>
							</div>
						</li> -->
					</ul>
				</div>
				<div class="col-lg-5 col-sm-12">
					<form class="header-form form-sm search_wrap pull-right">
						<input class="search-box" placeholder="Buscar Productos ..." required="" value="" type="search">
						<button type="submit">Buscar</button>
					</form>
				</div>

			</div>
		</div>
	</div>
	<div class="header-btm border-0 navy-dark-bg h-auto">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-9 col-12 m_menu">
					<div class="d-lg-none">
						<div class="form-captions" id="search">
							<button type="submit" class="submit-btn-2"><i class="fa fa-search"></i></button>
						</div>
						<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
					</div>
					<nav class="navbar navbar-expand-lg navbar-light">
						<div id="logo-navbar-fixed" style="display: none;">
							<img style="padding-right: 80px;" src="<?= base_url($this->session->userdata('logos')['shrinking-navbar']->resource);?>" alt="logo">
						</div>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav">
								<li class="nav-item">
									<a class="nav-link" href="<?= site_url(); ?>">Inicio</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="<?= site_url('aboutus_front'); ?>">Nosotros</a>
								</li>
								<!-- <li class="nav-item dropdown active">
									<a class="nav-link dropdown-toggler" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Home<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
										<div class="sub-menu dropdown-menu">
											<ul class="all-menu">
												<li><a href="index.html">Home Page 1</a></li>
												<li><a href="index-2.html">Home Page 2</a></li>
												<li class="active"><a href="index-3.html">Home Page 3</a></li>
												<li><a href="index-4.html">Home Page 4</a></li>
												<li><a href="index-5.html">Home Page 5</a></li>
												<li><a href="index-6.html">Home Page 6</a></li>
											</ul>
										</div>
								</li> -->

								<!-- <li class="dropdown">
									<a class="nav-link dropdown-toggler" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
									<div class="sub-menu dropdown-menu">
										<ul class="all-menu">
											<li><a href="error-404.html">404 Page</a></li>
											<li><a href="faq.html">Faq</a></li>
											<li><a href="coming-soon.html">Coming Soon</a></li>
										</ul>
									</div>
								</li> -->
								<!-- <li>
									<a class="nav-link dropdown-toggler" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Products<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
									<div class="sub-menu mega-menu dropdown-menu">
										<ul class="d-lg-flex">
											<li class="submenu-links dropdown col-lg-3">
												<a href="#" class="menu-title dropdown-toggler" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dresses<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
												<ul class="all-menu dropdown-menu">
													<li><a href="shop-three-columns.html">Cocktail</a></li>
													<li><a href="shop-four-columns.html">Day</a></li>
													<li><a href="shop-three-columns.html">Evening</a></li>
													<li><a href="shop-four-columns.html">Sundresses</a></li>
													<li><a href="shop-three-columns.html">Sweater</a></li>
													<li><a href="shop-four-columns.html">Belts</a></li>
												</ul>
											</li>
											<li class="submenu-links dropdown col-lg-3">
												<a href="#" class="menu-title dropdown-toggler" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Accessories<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
												<ul class="all-menu dropdown-menu">
													<li><a href="shop-three-columns.html">Evening</a></li>
													<li><a href="shop-four-columns.html">Long Sleeved</a></li>
													<li><a href="shop-three-columns.html">Sort Sleeved</a></li>
													<li><a href="shop-four-columns.html">Sleeveless</a></li>
													<li><a href="shop-three-columns.html">Tanks  and Camis</a></li>
													<li><a href="shop-four-columns.html">T-shirts</a></li>
												</ul>
											</li>
											<li class="submenu-links dropdown col-lg-3">
												<a href="#" class="menu-title dropdown-toggler" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tops<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
												<ul class="all-menu dropdown-menu">
													<li><a href="shop-three-columns.html">Cocktail</a></li>
													<li><a href="shop-four-columns.html">Day</a></li>
													<li><a href="shop-three-columns.html">Evening</a></li>
													<li><a href="shop-four-columns.html">Sundresses</a></li>
													<li><a href="shop-three-columns.html">Sweater</a></li>
													<li><a href="shop-four-columns.html">Belts</a></li>
												</ul>
											</li>
											<li class="submenu-links dropdown col-lg-3">
												<a href="#" class="menu-title dropdown-toggler" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Accessories<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
												<ul class="all-menu dropdown-menu">
													<li><a href="shop-three-columns.html">Evening</a></li>
													<li><a href="shop-four-columns.html">Long Sleeved</a></li>
													<li><a href="shop-three-columns.html">Sort Sleeved</a></li>
													<li><a href="shop-four-columns.html">Sleeveless</a></li>
													<li><a href="shop-three-columns.html">Tanks  and Camis</a></li>
													<li><a href="shop-four-columns.html">T-shirts</a></li>
												</ul>
											</li>
										</ul>
										<div class="d-sm-flex">
											<div class="col-sm-4">
												<a href="#" class="header-banner"><img src="image/header-banner-2.jpg" alt="header-banner"></a>
											</div>
											<div class="col-sm-4">
												<a href="#" class="header-banner"><img src="image/header-banner-3.jpg" alt="header-banner"></a>
											</div>
											<div class="col-sm-4">
												<a href="#" class="header-banner"><img src="image/header-banner-2.jpg" alt="header-banner"></a>
											</div>
										</div>
									</div>
								</li> -->
								<!-- <li>
									<a class="nav-link dropdown-toggler" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Shop<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
									<div class="sub-menu mega-menu dropdown-menu">
										<ul class="d-lg-flex">
											<li class="submenu-links dropdown col-lg-3">
												<a href="#" class="menu-title dropdown-toggler" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Shop Pages<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
												<ul class="all-menu dropdown-menu">
													<li><a href="shop-list.html">Shop List View</a></li>
													<li><a href="shop-grid.html">Shop Grid View</a></li>
													<li><a href="shop-three-columns.html">shop 3 Column</a></li>
													<li><a href="shop-four-columns.html">shop 4 Column</a></li>
													<li><a href="shop-left-sidebar.html">shop Left Sidebar</a></li>
													<li><a href="shop-right-sidebar.html">shop Right Sidebar</a></li>
												</ul>
											</li>
											<li class="submenu-links dropdown col-lg-3">
												<a href="#" class="menu-title dropdown-toggler" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Product Detail<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
												<ul class="all-menu dropdown-menu">
													<li><a href="product-detail.html">Product Default</a></li>
													<li><a href="product-detail-left-sidebar.html">Product Left Sidebar</a></li>
													<li><a href="product-detail-right-sidebar.html">Product Right Sidebar</a></li>
												</ul>
											</li>
											<li class="submenu-links dropdown col-lg-3">
												<a href="#" class="menu-title dropdown-toggler" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Extra<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
												<ul class="all-menu dropdown-menu">
													<li><a href="cart.html">Cart</a></li>
													<li><a href="checkout.html">Checkout</a></li>
													<li><a href="compare.html">Compare</a></li>
													<li><a href="my-account.html">My Account</a></li>
												</ul>
											</li>
											<li class="submenu-img col-lg-3"><a href="#"><img src="image/header-banner.jpg" alt="banner"></a></li>
										</ul>
									</div>
								</li> -->
								<!-- <li class="dropdown">
									<a class="nav-link dropdown-toggler" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Blog<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
									<div class="sub-menu dropdown-menu">
										<ul class="all-menu">
											<li><a class="dropdown-toggler" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Blog Layout<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
												<div class="sub-menu dropdown-menu">
													<ul class="all-menu">
														<li><a href="blog-standard-fullwidth.html">Blog Standard Fullwidth</a></li> 
														<li><a href="blog-standard-left-sidebar.html">Blog Standard Left Sidebar</a></li> 
														<li><a href="blog-standard-right-sidebar.html">Blog Standard Right Sidebar</a></li> 
														<li><a href="blog-three-columns.html">Blog 3 Columns </a></li>
														<li><a href="blog-four-columns.html">Blog 4 Columns</a></li>
														<li><a href="blog-grid-left-sidebar.html">Blog Grid Left Sidebar</a></li> 
														<li><a href="blog-grid-right-sidebar.html">Blog Grid Right Sidebar</a></li> 
													</ul>
												</div>
											</li>
											<li><a class="dropdown-toggler" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Blog Masonry<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
												<div class="sub-menu dropdown-menu">
													<ul class="all-menu">
														<li><a href="blog-masonry-three-columns.html">Masonry 3 Columns</a></li> 
														<li><a href="blog-masonry-four-columns.html">Masonry 4 Columns</a></li> 
														<li><a href="blog-masonry-left-sidebar.html">Masonry Left Sidebar </a></li>
														<li><a href="blog-masonry-right-sidebar.html">Masonry Right Sidebar</a></li>
													</ul>
												</div>
											</li>
											<li><a class="dropdown-toggler" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Blog List<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
												<div class="sub-menu dropdown-menu">
													<ul class="all-menu">
														<li><a href="blog-list-left-sidebar.html">List Left Sidebar </a></li>
														<li><a href="blog-list-right-sidebar.html">List Right Sidebar</a></li>
													</ul>
												</div>
											</li>
											<li><a class="dropdown-toggler" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Blog Single<i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></a>
												<div class="sub-menu dropdown-menu">
													<ul class="all-menu">
														<li><a href="blog-detail.html">Default</a></li>
														<li><a href="blog-detail-left-sidebar.html">Left Sidebar</a></li>
														<li><a href="blog-detail-right-sidebar.html">Right Sidebar</a></li>
														<li><a href="blog-detail-slider.html">Slider Post</a></li>
														<li><a href="blog-detail-video.html">Video post</a></li>
														<li><a href="blog-detail-audio.html">Audio Post</a></li>
													</ul>
												</div>
											</li>
										</ul>
									</div>
								</li> -->
								<li class="nav-item">
									<a class="nav-link" href="<?= site_url('contact_front'); ?>">Contáctenos</a>
								</li>
							</ul>
						</div>
					</nav>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-5 col-8 ml-auto pr-0">
					<div class="menu-wrap position-relative">
						<button type="button" class="categories-btn btn-block">
							<i class="ion-ios-list-outline"></i><span>Catálogos</span>
						</button>
						<nav id="mega-dropdown-menu" class="section-shadow">
							<ul class="list-unstyled components">
								<li>
									<a href="<?= base_url('uploads/tmp-pdf/TEMP-008-Catálogo Halloween.pdf'); ?>" target="_blank">Halloween</a>
								</li>
								<li>
									<a href="<?= base_url('uploads/tmp-pdf/TEMP-009-Cátalogo Artículos Promocionales Navidad.pdf'); ?>" target="_blank">Promocionales de Navidad</a>
								</li>
								<li>
									<a href="<?= base_url('uploads/tmp-pdf/TEMP-010-Catálogo-Regalos Corporativos Navidad.pdf'); ?>" target="_blank">Corporativos de Navidad</a>
								</li>
								<!-- <li>
									<a class="dropdown-toggler" href="#" data-toggle="dropdown" aria-expanded="false">shirt<i class="ion-plus-round plus-icon"></i><i class="ion-minus-round minus-icon"></i></a>
									<ul class="dropdown-menu list-unstyled">
										<li>
											<a href="#">The Crew Neck shirt</a>
										</li>
										<li>
											<a href="#">The V Nec shirt</a>
										</li>
										<li>
											<a href="#">The Henley - Y neck shirt</a>
										</li>
										<li>
											<a href="#">Polo t shirt collar shirt</a>
										</li>
									</ul>
								</li>
								<li>
									<a class="dropdown-toggler" href="#" data-toggle="dropdown" aria-expanded="false">t-shirt<i class="ion-plus-round plus-icon"></i><i class="ion-minus-round minus-icon"></i></a>
									<ul class="dropdown-menu list-unstyled">
										<li>
											<a href="#">The Scoop Neck style t-Shirt</a>
										</li>
										<li>
											<a href="#">Polo t shirt collar styles t-Shirt</a>
										</li>
										<li>
											<a href="#">Raglan sleeve types t-Shirt</a>
										</li>
										<li>
											<a href="#">Sleeveless t-Shirt</a>
										</li>
										<li>
											<a href="#">Cap sleeves t-Shirt</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="#">party suit</a>
								</li>
								<li>
									<a class="dropdown-toggler" href="#" data-toggle="dropdown" aria-expanded="false">women's fashion<i class="ion-plus-round plus-icon"></i><i class="ion-minus-round minus-icon"></i></a>
									<ul class="dropdown-menu list-unstyled">
										<li>
											<a href="#">Skirts</a>
										</li>
										<li>
											<a href="#">Dresses </a>
										</li>
										<li>
											<a href="#">Westarn Wear</a>
										</li>
										<li>
											<a href="#">Ethic Wear</a>
										</li>
										<li>
											<a href="#">Sport Wear</a>
										</li>
									</ul>
								</li>
								<li>
									<a class="dropdown-toggler" href="#" data-toggle="dropdown" aria-expanded="false">man's fashion<i class="ion-plus-round plus-icon"></i><i class="ion-minus-round minus-icon"></i></a>
									<ul class="dropdown-menu list-unstyled">
										<li>
											<a href="#">Sports Wear</a>
										</li>
										<li>
											<a href="#">Western Wear</a>
										</li>
										<li>
											<a href="#">Ethic Wear</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="#">summer cloths</a>
								</li>
								<li>
									<a href="#">monsoon cloths</a>
								</li>
								<li>
									<a href="#">winter cloths</a>
								</li> -->
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- End Header Section -->