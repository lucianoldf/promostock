<?= get_message_from_operation(); ?>
<!-- Start Breadcrumbs Section -->
<!-- <section class="breadcrumbs-section background_bg" data-img-src="image/pd-breadcrumbs-img.jpg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page_title text-center">
                    <h1>Detalle del Producto</h1>
                    <ul class="breadcrumb justify-content-center">
                        <li><a href="index.html">home</a></li>
                        <li><a href="#">Shop</a></li>
                        <li><span>Product Detail</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section> -->
<!-- End Header Section -->

<!-- Start Product Detail Section -->
<section class="products-detail-section pt_large">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="product-image">
                    <img class="product_img" src='<?= base_url($product->resource_main); ?>' data-zoom-image="<?= base_url($product->resource_main); ?>" />
                </div>
                <div id="pr_item_gallery" class="product_gallery_item owl-thumbs-slider owl-carousel owl-theme">
                    <div class="item">
                        <a href="#" class="active" data-image="<?= base_url($product->resource_main); ?>" data-zoom-image="<?= base_url($product->resource_main); ?>">
                            <img src="<?= base_url($product->resource_small); ?>" />
                        </a>
                    </div>
                    <!-- <div class="item">
                    <a href="#" data-image="image/product2.png" data-zoom-image="image/product2.png">
                        <img src="image/product_small2.png" />
                    </a>
                </div> -->
                </div>
            </div>
            <div class="col-md-7">
                <div class="quickview-product-detail">
                    <h2 class="box-title"><?= $product->name; ?></h2>
                    <!-- <h3 class="box-price"><del>$ 95.00</del>$ 81.00</h3> -->
                    <h3 class="box-price"><?= SIMBOL_MONEY . " " . $product->price_final; ?></h3>
                    <p class="box-text"><?= $product->description; ?></p>
                    <?php if ($product->stock_quantity > 0) { ?>
                        <p class="stock">Disponibilidad: <span>Disponible</span></p>
                    <?php } else { ?>
                        <p class="stock">Disponibilidad: <span>No disponible</span></p>
                    <?php } ?>

                    <div class="quantity-box">
                        <p>Cantidad:</p>
                        <div class="input-group">
                            <input type="button" value="-" class="minus">
                            <input class="quantity-number qty" type="text" value="1" min="1" max="10">
                            <input type="button" value="+" class="plus">
                        </div>
                        <div class="quickview-cart-btn">
                            <a href="#" class="btn btn-primary"><img src="<?= base_url(); ?>assets/templates/atz_shop/image/cart-icon-1.png" alt="cart-icon-1"> Agregar al carrito</a>
                        </div>
                    </div>
                    <div class="box-social-like d-sm-flex justify-content-between">
                        <!-- <ul class="hover-icon box-like">
                            <li><a href="#"><i class="fa fa-heart"></i></a></li>
                            <li><a href="#"><i class="fa fa-refresh"></i></a></li>
                        </ul> -->
                        <!-- <ul class="hover-icon box-social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Product Detail Section -->

<!-- Start Product Tabs Section -->
<section class="products-detail-tabs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="products-tabs">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="discription-tab" data-toggle="tab" href="#discription" role="tab" aria-controls="discription" aria-selected="true">Descripcion</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ai-tab" data-toggle="tab" href="#ai" role="tab" aria-controls="ai" aria-selected="false">INFORMACION ADICIONAL</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">REVISIONES (2)</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade tab-1 show active" id="discription" role="tabpanel" aria-labelledby="discription-tab">
                            <div class="tab-title">
                                <h6>Descripción</h6>
                            </div>
                            <div class="tab-caption">
                                <p><?= $product->description; ?></p>
                            </div>
                        </div>
                        <div class="tab-pane fade tab-2" id="ai" role="tabpanel" aria-labelledby="ai-tab">
                            <div class="tab-title">
                                <h6>Información Adicional</h6>
                            </div>
                            <div class="tab-caption">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td colspan="1">Brand</td>
                                                <td>FairPlay</td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">Season</td>
                                                <td>Spring / Summer</td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">Color</td>
                                                <td>Black</td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">Fit</td>
                                                <td>Comfort</td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">Size</td>
                                                <td>S</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade tab-3" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                            <div class="tab-title">
                                <h6>Revisiones de clientes</h6>
                            </div>
                            <div class="tab-caption">
                                <div class="costomer-reviews">
                                    <?php if ($product_reviews) { ?>
                                        <?php foreach ($product_reviews as $review) { ?>
                                            <div class="costomer-reviews-box">
                                                <div class="reviews-img">
                                                    <img style="width: 10%; height: 10%;" src="<?= base_url($review->user_photo); ?>" alt="costomer-img">
                                                </div>
                                                <div class="reviews-text" style="position: absolute; left: 15%;">
                                                    <p class="reviewer-name"><?= $review->user_name; ?></p>
                                                    <span class="reviews-date"><?= date('d/m/Y', $review->created_at); ?></span>
                                                    <p class="reviewer-text"><?= $review->text_review; ?></p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <p>Sin revisiones</p>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="tab-caption">
                                <div class="add-review">
                                    <div class="tab-title">
                                        <h6>Agregar comentario</h6>
                                    </div>
                                    <form id="form-product-review" method="POST" class="add-review-form" action="<?= site_url('product_detail/save_review'); ?>">
                                        <input type="hidden" name="product_id" value="<?= $product->_id; ?>">
                                        <div class="input-1">
                                            <input required class="form-control" name="user_name" placeholder="<?= translate('ph_review_user_name'); ?>" value="<?= ($this->session->userdata('name')) ? $this->session->userdata('name') : "";?>" type="text">
                                        </div>
                                        <div class="input-2">
                                            <input required class="form-control" name="user_email" placeholder="<?= translate('ph_review_user_email'); ?>" value="<?= ($this->session->userdata('email')) ? $this->session->userdata('email') : "";?>" type="email">
                                        </div>
                                        <div class="input-3">
                                            <textarea required rows="6" class="form-control" name="text_review" placeholder="<?= translate('ph_review_text'); ?>"></textarea>
                                        </div>
                                        <div class="input-btn">
                                            <button type="submit" class="btn btn-secondary"><?= translate('bt_review_submit'); ?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Product Tabs Section -->

<!-- Start Related Product Section -->
<section class="related-product pb_large">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h4>PRODUCTOS RELACIONADOS</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="products-slider4 same-nav owl-carousel owl-theme" data-margin="30" data-dots="false">
                    <?php if ($products_related) foreach ($products_related as $product) { ?>
                        <div class="item">
                            <div class="product-box common-cart-box">
                                <div class="product-img common-cart-img">
                                    <img src="<?= base_url($product->resource_catalog); ?>" alt="product-img">
                                    <div class="hover-option">
                                        <!-- <div class="add-cart-btn">
                                        <a href="#" class="btn btn-primary">Add To Cart</a>
                                    </div> -->
                                        <ul class="hover-icon">
                                            <li><a href="#"><i class="fa fa-cart-plus" aria-hidden="true"></i></a></li>
                                            <!-- <li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a></li> -->
                                            <li><a href="#popup-detail-product" id="<?= $product->_id; ?>" class="quickview-popup-link"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                            <!-- <li><a href="#"><i class="fas fa-refresh"></i></a></li> -->
                                        </ul>
                                    </div>
                                </div>
                                <div class="product-info common-cart-info">
                                    <a href="<?= site_url('product_detail/' . $product->_id); ?>" class="cart-name"><?= $product->name; ?></a>
                                    <!-- <p class="cart-price"><del>10</del>5</p> -->
                                    <p class="cart-price"><?= SIMBOL_MONEY . " " . number_format($product->price_final, 2); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Related Product Section -->