<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('category_prod_config_lang'); ?>
            <small><?= translate('edit_category_prod_lang'); ?></small>
            | <a href="<?= site_url('category_product/index'); ?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> <?= translate('back_lang'); ?>
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('edit_category_prod_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('edit_category_prod_lang'); ?></h3>
                    </div>
                    <div class="box-body">

                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart("category_product/update"); ?>

                        <input type="hidden" value="active" name="is_active">
                        <input id="id-banner" type="hidden" name="category_id" value="<?= $category_prod_object->_id; ?>">
                        <input id="old-file" type="hidden" name="old-file" value="<?= $category_prod_object->resource; ?>">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label><?= translate("category_prod_text_label_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="text" maxlength="255" class="form-control" name="text" placeholder="<?= translate('category_prod_text_ph_lang'); ?>" value="<?= $category_prod_object->name; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><?= translate("image_label_lang"); ?> (<?= "$img_width X $img_height"; ?>)</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                            <input id="image-upload" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" name="archivo">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 col-lg-offset-3">
                                        <?php if (strlen($category_prod_object->resource) > 0) { ?>
                                            <img style="width: 50%;" class="img img-rounded img-responsive" src="<?= base_url($category_prod_object->resource); ?>" />
                                        <?php } ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: left;">
                                        <button id="btn-submit-form" type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <?= form_close(); ?>
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">

</script>