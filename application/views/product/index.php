<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('product_config_lang'); ?>
            <small><?= translate('product_config_lang'); ?></small>
            | <a href="<?= site_url('product/add_index'); ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?= translate('add_item_lang'); ?>
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('product_list_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?= translate('product_list_lang'); ?></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <?= get_message_from_operation(); ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><?= translate("code_table_title_lang"); ?></th>
                                    <th><?= translate("name_table_title_lang"); ?></th>
                                    <!-- <th><?= translate("category_table_title_lang"); ?></th> -->
                                    <th><?= translate("image_table_title_lang"); ?></th>
                                    <th><?= translate("status_table_title_lang"); ?></th>
                                    <th><?= translate("actions_table_title_lang"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($products) { ?>
                                    <?php foreach ($products as $item) { ?>
                                        <tr>
                                            <td><?= $item->code; ?></td>
                                            <td><?= $item->name; ?></td>
                                            <!-- <td><?= $item->category; ?></td> -->
                                            <td>
                                                <img style="margin:0 auto; width: 80px; height: 80px;" class="img img-rounded img-responsive" src="<?= base_url($item->resource_main); ?>" />
                                            </td>
                                            <td>
                                                <?php if ($item->is_active == 1) { ?>
                                                    <h5 class="text-green"><i class="fa fa-check"></i> Activo</h5>
                                                <?php } ?>
                                                <?php if ($item->is_active == 0) { ?>
                                                    <h5 class="text-yellow"><i class="fa fa-ban"></i> Inactivo</h5>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <!-- Single button -->
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Acciones <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a class="text-blue" href="<?= site_url('product/update_index/' . $item->_id); ?>"><i class="fa fa-edit"></i> <?= translate("edit_btn_lang"); ?></a></li>
                                                        <?php if ($item->is_active == 1) { ?>
                                                            <li><a class="text-yellow" href=<?= site_url("product/update_status/0/{$item->_id}"); ?>><i class="fa fa-ban"></i> <?= translate("deactive_btn_lang"); ?></a></li>
                                                        <?php } ?>
                                                        <?php if ($item->is_active == 0) { ?>
                                                            <li><a class="text-green" href=<?= site_url("product/update_status/1/{$item->_id}"); ?>><i class="fa fa-check"></i> <?= translate("active_btn_lang"); ?></a></li>
                                                        <?php } ?>
                                                        <li><a class="text-red" data-toggle="modal" data-target="#modal-delete" data-action="<?= site_url('product/delete'); ?>" data-id="<?= $item->_id; ?>" href=""><i class="fa fa-remove"></i> <?= translate("delete_btn_lang"); ?></a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th><?= translate("code_table_title_lang"); ?></th>
                                    <th><?= translate("name_table_title_lang"); ?></th>
                                    <!-- <th><?= translate("category_table_title_lang"); ?></th> -->
                                    <th><?= translate("image_table_title_lang"); ?></th>
                                    <th><?= translate("status_table_title_lang"); ?></th>
                                    <th><?= translate("actions_table_title_lang"); ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>
    $(function() {
        $(".modal-title").text('<?= translate('title_alert_product_delete'); ?>');
        $(".modal-body p").text('<?= translate('message_alert_product_delete'); ?>');

        $("#example1").DataTable({
            "order": [
                // [3, "asc"],
                [1, "asc"],
            ],
            "ordering": true,
            "columnDefs": [{
                "className": "text-center",
                "targets": "_all",
            }],
        });
    });
</script>