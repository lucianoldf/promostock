<style>
    .resume-upload {
        border-radius: 2px;
        max-height: 150px;
        min-height: 33px;
        overflow: auto;
        margin: 0;
        border: 1px solid #000;
        width: auto;
        padding: 2px 30px 2px 5px;
        background-color: #373736;
        color: white;
    }

    .modal {}

    .modal img {
        width: 100px;
        height: auto;
        margin: 0 auto;
        left: 50%;
        position: absolute;
        top: 50%;

    }
</style>
<div id="modal-loading" class="modal" tabindex="-1" role="dialog" style="display: none;">
    <img src="<?= base_url('assets/images/loading.gif'); ?>">
</div>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('product_import_config_lang'); ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('product_import_config_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('product_import_config_lang'); ?></h3>
                    </div>
                    <div class="box-body">
                        <div id="alert-message" class="alert alert-danger alert-dismissable" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> <?= translate('title_alert_message_lang'); ?></h4>
                            <p></p>
                        </div>

                        <?= get_message_from_operation(); ?>

                        <form enctype="multipart/form-data" id="formUploadSpreadsheet" method="post">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label><?= translate("product_label_import_file_lang"); ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                        <input id="import-upload" type="file" required accept=".xlsx" class="form-control" name="file-import">
                                    </div>
                                </div>
                                <div class="col-lg-2" style="text-align: left; margin-top: 25px;">
                                    <button id="submit-import-spreadsheet" type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                                </div>
                                <div class="col-lg-6">
                                    <label><?= translate("product_label_status_lang"); ?></label>
                                    <div id="resume-upload" class="resume-upload" contenteditable></div>
                                </div>
                            </div>
                            <br>
                        </form>
                        <form enctype="multipart/form-data" id="formUploadZip" method="post">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label><?= translate("product_label_import_zip_lang"); ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                        <input id="import-zip-upload" type="file" required accept=".zip" class="form-control" name="file-zip-import">
                                    </div>
                                </div>
                                <div class="col-lg-2" style="text-align: left; margin-top: 25px;">
                                    <button id="submit-import-zip" type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                                </div>
                                <div class="col-lg-6">
                                    <label><?= translate("product_label_status_zip_lang"); ?></label>
                                    <div id="resume-upload-zip" class="resume-upload" contenteditable></div>
                                </div>
                            </div>
                        </form>
                        <!-- <br>
                            <div class="row">
                                <div class="col-xs-12" style="text-align: left;">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                                </div>
                            </div> -->
                        <!-- </form> -->
                        <!-- <?= form_close(); ?> -->
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
</div><!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    let color_error = "#DD4B39";
    let color_success = "#6CC72B";

    $(function() {
        // var example = "Texto de <strong>ejemplo</strong> en textarea \n salto";
        // $("#resume-upload").html(example);

        $('#formUploadSpreadsheet').on('submit', function(e) {
            e.preventDefault();
            importSpreadsheetProducts();
        });

        $('#formUploadZip').on('submit', function(e) {
            e.preventDefault();
            importZipImagesOfProducts();
        });

        // $("#submit-import-spreadsheet").on('click', function(e) {
        //     e.preventDefault();
        //     importSpreadsheetProducts();
        // });

        // $("#submit-import-zip").on('click', function(e) {
        //     e.preventDefault();
        //     console.log('import zip');
        //     importZipImagesOfProducts();
        // });

        /**
         * Upload and import list of products
         */
        let importSpreadsheetProducts = function() {
            console.log('para hacer upload xlsx!');

            let data_send = new FormData();

            let file_import = $('input[name=file-import]')[0].files[0];

            if (file_import === undefined) {
                showMessage('Debe cargar un archivo para improtar');
                $('input[name=file-import]').val('');
                return 0;
            }

            data_send.append('file-import', file_import);
            $(".modal").show();
            $.ajax({
                url: '<?= site_url('import_products/import_products'); ?>',
                data: data_send,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                success: function(data) {
                    $(".modal").hide();

                    console.log(data);

                    data = JSON.parse(data);

                    console.log(data);

                    data.msg = data.msg.replace(/\OK/g, '<span style="color:' + color_success + ';"><i class="fa fa-check"></i></span>');
                    data.msg = data.msg.replace(/\ERROR/g, '<strong style="color:' + color_error + ';"><i class="fa fa-times"></i></strong>');
                    data.msg = data.msg.replace(/\Resumen:/g, '<br><strong>Resumen:</strong><br>');


                    console.log(data);

                    $("#resume-upload").html(data.msg);

                    $('input[name=file-import]').val('');
                }
            });
        };



        /**
         * Upload and import imagenes de productos
         */
        let importZipImagesOfProducts = function() {
            console.log('para hacer upload zip!');

            let data_send = new FormData();

            let file_import = $('input[name=file-zip-import]')[0].files[0];

            if (file_import === undefined) {
                showMessage('Debe cargar un archivo para improtar');
                $('input[name=file-zip-import]').val('');
                return 0;
            }

            data_send.append('file-zip-import', file_import);

            $(".modal").show();

            $.ajax({
                url: '<?= site_url('import_products/import_images_products'); ?>',
                data: data_send,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                success: function(data) {
                    $(".modal").hide();
                    data = JSON.parse(data);
                    console.log(data.msg);
                    data.msg = data.msg.replace(/\OK/g, '<span style="color:' + color_success + ';"><i class="fa fa-check"></i></span>');
                    data.msg = data.msg.replace(/\ERROR/g, '<strong style="color:' + color_error + ';"><i class="fa fa-times"></i></strong>');
                    data.msg = data.msg.replace(/\Resumen:/g, '<br><strong>Resumen:</strong><br>');

                    // console.log(data);

                    $("#resume-upload-zip").html(data.msg);
                    $('input[name=file-zip-import]').val('');
                }
            });
        };

    });
</script>