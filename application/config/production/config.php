<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Define CONFIG configuration for PRODUCTION enviroment
*/
/**
 * TODO: Modificar configuracion con dominio de produccion
 */
/* Local config */
//$config['base_url'] = 'http://prod.promostock.local/';

/* Remote config test enviroment*/
//$config['base_url'] = 'http://promostock.datalabcenter.com/';

/* Remote config produccion rela*/
$config['base_url'] = 'http://promostock.com.ec/';


//$config['index_page'] = '';

//$config['uri_protocol']	= 'REQUEST_URI';

// Solo para testear como seria en produccion
//$config['sess_save_path'] = '/tmp/www';
$config['log_threshold'] = 0;
$config['log_file_extension'] = 'log';


// Enable hooks
$config['enable_hooks'] = TRUE;

