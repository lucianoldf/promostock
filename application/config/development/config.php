<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Define CONFIG configuration for DEVELOPMENT DEFAULT enviroment
*/

/* Local */
$config['base_url'] = 'http://dev.promostock.local/';

//$config['index_page'] = '';

//$config['uri_protocol']	= 'REQUEST_URI';

// Logs
$config['log_threshold'] = array(1,2);
$config['log_file_extension'] = 'log';

// Enable hooks
$config['enable_hooks'] = TRUE;
