<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Define CONFIG configuration for DEVELOPMENT OSX enviroment
*/

/* Local config */
$config['base_url'] = 'http://dev.osx.local/bp.ci.3.1.11';

/* Remote config */

//$config['index_page'] = '';

//$config['uri_protocol']	= 'REQUEST_URI';

$config['sess_save_path'] = '/tmp/www';
$config['log_threshold'] = 4;
$config['log_file_extension'] = 'log';
