<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Define CONFIG configuration for DEVELOPMENT DEFAULT enviroment
*/

/* Local config */
// $config['base_url'] = 'http://demo.promostock.local/';

/* Remote config */
$config['base_url'] = 'http://demo.promostock.datalabcenter.com/';

//$config['index_page'] = '';

//$config['uri_protocol']	= 'REQUEST_URI';

$config['log_threshold'] = 0;
$config['log_file_extension'] = 'log';


