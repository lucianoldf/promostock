<?php

class Ourclient extends CI_Controller
{

    private const WIDHT = 100;
    private const HEIGHT = 100;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('OurClient_model', 'ourcli');

        @session_start();
        $this->init_form_validation();

    }

    /****************************************************************
    * SHOW VIEWS
    ****************************************************************/
    // Show List description
    public function index_description()
    {
        $this->validate_rol([1]);

        $description = $this->ourcli->get_description_by_id('description');
        $data['description'] = $description;
        $this->load_view_admin_g("our_client/index_description", $data);
    }



    // Show List our clients
    public function index_ourclient()
    {
        $this->validate_rol([1]);

        $all_ourclients = $this->ourcli->get_all_ourclient();
        $data['all_ourclients'] = $all_ourclients;
        $this->load_view_admin_g("our_client/index_ourclient", $data);
    }



    // Show Add description
    public function index_description_add()
    {
        $this->validate_rol([1]);

        $this->load_view_admin_g('our_client/add_description');
    }



    // Show Update description
    function index_description_update($our_cli_id = 0)
    {
        $this->validate_rol([1]);

        $our_cli_des_object = $this->ourcli->get_description_by_id($our_cli_id);

        if ($our_cli_des_object) {

            $data['our_cli_des_object'] = $our_cli_des_object;
            $this->load_view_admin_g('our_client/update_description', $data);
        } else {

            show_404();
        }
    }



    // Show Add ourclient
    public function index_ourclient_add()
    {
        $this->validate_rol([1]);

        $this->load_view_admin_g('our_client/add_ourclient');
    }



    // Show Update oruclient
    function index_ourclient_update($our_cli_id = 0)
    {
        $this->validate_rol([1]);

        $ourclient_object = $this->ourcli->get_ourclient_by_id($our_cli_id);

        if ($ourclient_object) {

            $data['ourclient_object'] = $ourclient_object;
            $this->load_view_admin_g('our_client/update_ourclient', $data);
        } else {

            show_404();
        }
    }




    /****************************************************************
    * ACTIONS
    ****************************************************************/

    // Action Add description
    public function add_description()
    {
        $this->validate_rol([1]);

        // Si ya existe cargada una descripcion no se permite cargar mas
        if( $this->ourcli->get_description_by_id('description') ){
            $this->response->set_message(translate("message_max_description_our_clients"), ResponseMessage::ERROR);
            redirect("ourclient/index_description", "location", 301);
        }
        
        $title = $this->input->post('title');
        $description = $this->input->post('description');

        $data = [
            'title' => $title,
            'description' => $description,
            'is_active' => 1
        ];

        $this->ourcli->create_description($data);
        $this->response->set_message(translate("message_save_des_our_clients_ok"), ResponseMessage::SUCCESS);
        redirect("ourclient/index_description", "location", 301);
    }


    // Action Update description
    public function update_description()
    {
        $this->validate_rol([1]);

        $title = $this->input->post('title');
        $description = $this->input->post('description');
        $our_cli_id = 'description';

        $data = [
            'title' => $title,
            'description' => $description,
        ];

        $this->ourcli->update_description($our_cli_id, $data);
        $this->response->set_message(translate("message_update_des_our_clients_ok"), ResponseMessage::SUCCESS);
        redirect("ourclient/index_description", "location", 301);
    }


    // Action Update Status description
    public function update_status_description($status, $id)
    {
        $this->validate_rol([1]);

        $data = [
            'is_active' => $status,
        ];
        
        $r = $this->ourcli->update_description($id, $data);
        if ($r === false) {

            $this->response->set_message(translate("message_error"), ResponseMessage::SUCCESS);
            redirect("ourclient/index_description/", "location", 301);
        } else {

            $this->response->set_message(translate("message_update_des_our_clients_ok"), ResponseMessage::SUCCESS);
            redirect("ourclient/index_description/", "location", 301);
        }
    }


    // Action Add ourclient
    public function add_ourclient()
    {
        $this->validate_rol([1]);

        $name = $this->input->post('name');
        $comment = $this->input->post('comment');

        $data = [
            'name' => $name,
            'comment' => $comment,
            'resource' => null,
            'is_active' => 1
        ];

        $name_file = $_FILES['archivo']['name'];
        $separado = explode('.', $name_file);
        $ext = end($separado); // me quedo con la extension
        $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
        $allow_extension = in_array($ext, $allow_extension_array);

        if ($allow_extension) {

            $result = save_image_from_post('archivo', './uploads/our_clients', time(), $this::WIDHT, $this::HEIGHT);
            if ($result[0]) {

                $data['resource'] = $result[1];

                $this->ourcli->create_ourclient($data);
                $this->response->set_message(translate("message_save_our_clients_ok"), ResponseMessage::SUCCESS);
                redirect("ourclient/index_ourclient", "location", 301);
            } else {

                if( $_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE ) $result[1] = translate('message_file_size_exceeded_php_ini');
                
                $this->response->set_message($result[1], ResponseMessage::ERROR);
                redirect("ourclient/index_ourclient_add", "location", 301);
            }
        } else {

            $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
            redirect("ourclient/index_ourclient_add", "location", 301);
        }
    }


    // Action Update ourclient
    public function update_ourclient()
    {
        $this->validate_rol([1]);

        $name = $this->input->post('name');
        $comment = $this->input->post('comment');
        $our_cli_id = $this->input->post('our-cli-id');
        $old_file = $this->input->post('old-file');

        $data = [
            'name' => $name,
            'comment' => $comment,
        ];

        $update_file = FALSE;

        $name_file = $_FILES['archivo']['name'];
        $ofile_name = explode('/', $old_file);

        if (end($ofile_name) != $name_file and $name_file != null) {
            $update_file = TRUE;
        }

        if (!$update_file) {

            $r = $this->ourcli->update_ourclient($our_cli_id, $data);

            if ($r === false) {

                $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                redirect("ourclient/index_ourclient", "location", 301);
            } else {

                $this->response->set_message(translate("message_update_our_clients_ok"), ResponseMessage::SUCCESS);
                redirect("ourclient/index_ourclient", "location", 301);
            }
        } else {

            $separado = explode('.', $name_file);
            $ext = end($separado); // me quedo con la extension
            $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
            $allow_extension = in_array($ext, $allow_extension_array);

            if ($allow_extension) {

                $result = save_image_from_post('archivo', './uploads/our_clients', time(), $this::WIDHT, $this::HEIGHT);
                if ($result[0]) {
                    if ($old_file != null) {
                        unlink($old_file);
                    }

                    $data['resource'] = $result[1];

                    $r = $this->ourcli->update_ourclient($our_cli_id, $data);
                    if ($r > 0) {
                        $this->response->set_message(translate("message_save_logo_ok"), ResponseMessage::SUCCESS);
                        redirect("ourclient/index_ourclient", "location", 301);
                    } else {
                        $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                        redirect("ourclient/index_ourclient", "location", 301);
                    }
                } else {

                    if( $_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE ) $result[1] = translate('message_file_size_exceeded_php_ini');

                    $this->response->set_message($result[1], ResponseMessage::ERROR);
                    redirect("ourclient/index_ourclient", "location", 301);
                }
            } else {

                $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
                redirect("ourclient/index_ourclient", "location", 301);
            }
        }
    }



    // Action Update Status ourclient
    public function update_status_ourclient($status, $id)
    {
        $this->validate_rol([1]);

        $data = [
            'is_active' => $status,
        ];
        
        $r = $this->ourcli->update_ourclient($id, $data);
        if ($r === false) {

            $this->response->set_message(translate("message_error"), ResponseMessage::SUCCESS);
            redirect("ourclient/index_ourclient", "location", 301);
        } else {

            $this->response->set_message(translate("message_update_our_clients_ok"), ResponseMessage::SUCCESS);
            redirect("ourclient/index_ourclient", "location", 301);
        }
    }



    // Action Delete ourclient
    public function delete_ourclient($our_cli_id = 0)
    {
        $this->validate_rol([1]);

        $logo_object = $this->ourcli->get_ourclient_by_id($our_cli_id);

        if ($logo_object) {

            if (file_exists($logo_object->resource)) {
                unlink($logo_object->resource);
            }

            $this->ourcli->delete_ourclient($our_cli_id);

            $this->response->set_message(translate('message_delete_our_clients_ok'), ResponseMessage::SUCCESS);
            redirect("ourclient/index_ourclient", "location", 301);
        } else {

            $this->response->set_message(translate('message_error'), ResponseMessage::SUCCESS);
            redirect("ourclient/index_ourclient", "location", 301);
        }
    }
}
