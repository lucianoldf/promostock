<?php

class Banner extends CI_Controller
{

    private const WIDTH = 1900;
    private const HEIGHT = 350;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Banner_model', 'banner');

        @session_start();
        $this->init_form_validation();
    }


    public function index()
    {

        $this->validate_rol([1]);

        $all_banners = $this->banner->get_all();
        $data['all_banners'] = $all_banners;
        $this->load_view_admin_g("banner/index", $data);
    }



    public function add_index()
    {
        $this->validate_rol([1]);

        $nex_order = $this->banner->get_last_order();
        $data['next_order'] = $nex_order;
        $this->load_view_admin_g('banner/add', $data);
    }



    public function add()
    {
        $this->validate_rol([1]);

        $title = $this->input->post('title');
        $subtitle = $this->input->post('subtitle');
        $order = $this->input->post('order');
        $btn_link = $this->input->post('button-link');
        $alig_text = $this->input->post('align-text');
        $btn_name = $this->input->post('button-name');
        $color_text = $this->input->post('color-text');

        $data = [
            'title' => $title,
            'subtitle' => $subtitle,
            'color_text' => $color_text,
            'align_text' => $alig_text,
            'btn_name' => $btn_name,
            'btn_link' => $btn_link,
            'order' => ($order != null) ? (int)$order : 0,
            'resource' => null,
            'is_active' => 0
        ];

        $name_file = $_FILES['archivo']['name'];
        $separado = explode('.', $name_file);
        $ext = strtolower( end($separado) ); // me quedo con la extension
        $allow_extension = in_array($ext, ALLOW_EXTENSION_UPLOAD_IMAGE);

        if ($allow_extension) {

            if($ext == 'gif'){
                $result = save_image_from_post('archivo', './uploads/banner', time(), 0, 0);
            }else{
                $result = save_image_from_post('archivo', './uploads/banner', time(), $this::WIDTH, $this::HEIGHT);
            }
            
            if ($result[0]) {

                $data['resource'] = $result[1];

                $this->banner->create($data);
                $this->response->set_message(translate("message_save_banner_ok"), ResponseMessage::SUCCESS);
                redirect("banner/index", "location", 301);
            } else {

                if ($_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE) $result[1] = translate('message_file_size_exceeded_php_ini');

                $this->response->set_message($result[1], ResponseMessage::ERROR);
                redirect("banner/add_index", "location", 301);
            }
        } else {

            $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
            redirect("banner/add_index/", "location", 301);
        }
    }


    function update_index($banner_id = 0)
    {
        $this->validate_rol([1]);

        $banner_object = $this->banner->get_by_id($banner_id);

        if ($banner_object) {

            $data['banner_object'] = $banner_object;
            $this->load_view_admin_g('banner/update', $data);
        } else {

            show_404();
        }
    }



    public function update_status($status, $id)
    {
        $this->validate_rol([1]);

        $data = [
            'is_active' => (int)$status,
        ];

        $r = $this->banner->update($id, $data);
        if ($r === false) {

            $this->response->set_message(translate("message_error"), ResponseMessage::SUCCESS);
            redirect("banner/index/", "location", 301);
        } else {

            $this->response->set_message(translate("message_update_banner_ok"), ResponseMessage::SUCCESS);
            redirect("banner/index/", "location", 301);
        }
    }



    public function update()
    {
        $this->validate_rol([1]);

        $title = $this->input->post('title');
        $subtitle = $this->input->post('subtitle');
        $order = $this->input->post('order');
        $btn_link = $this->input->post('button-link');
        $alig_text = $this->input->post('align-text');
        $btn_name = $this->input->post('button-name');
        $color_text = $this->input->post('color-text');
        
        $banner_id = $this->input->post('banner_id');
        $old_file = $this->input->post('old-file');
        
        $data = [
            'title' => $title,
            'subtitle' => $subtitle,
            'color_text' => $color_text,
            'align_text' => $alig_text,
            'btn_name' => $btn_name,
            'btn_link' => $btn_link,
            'order' => ($order != null) ? (int)$order : 0
        ];

        $update_file = FALSE;

        $name_file = $_FILES['archivo']['name'];
        $ofile_name = explode('/', $old_file);

        if (end($ofile_name) != $name_file and $name_file != null) {
            $update_file = TRUE;
        }

        if (!$update_file) {

            $r = $this->banner->update($banner_id, $data);

            if ($r === false) {

                $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                redirect("banner/index/", "location", 301);
            } else {

                $this->response->set_message(translate("message_update_banner_ok"), ResponseMessage::SUCCESS);
                redirect("banner/index/", "location", 301);
            }
        } else {

            $separado = explode('.', $name_file);
            $ext = strtolower( end($separado) ); // me quedo con la extension
            $allow_extension = in_array($ext, ALLOW_EXTENSION_UPLOAD_IMAGE);

            if ($allow_extension) {

                if($ext == 'gif'){
                    $result = save_image_from_post('archivo', './uploads/banner', time(), 0, 0);
                }else{
                    $result = save_image_from_post('archivo', './uploads/banner', time(), $this::WIDTH, $this::HEIGHT);
                }

                if ($result[0]) {
                    if ($old_file != null) {
                        unlink($old_file);
                    }

                    $data['resource'] = $result[1];

                    $r = $this->banner->update($banner_id, $data);
                    if ($r > 0) {
                        $this->response->set_message(translate("message_save_banner_ok"), ResponseMessage::SUCCESS);
                        redirect("banner/index/", "location", 301);
                    } else {
                        $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                        redirect("banner/index/", "location", 301);
                    }
                } else {

                    if ($_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE) $result[1] = translate('message_file_size_exceeded_php_ini');

                    $this->response->set_message($result[1], ResponseMessage::ERROR);
                    redirect("banner/index/", "location", 301);
                }
            } else {

                $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
                redirect("banner/index/", "location", 301);
            }
        }
    }



    public function delete($banner_id = 0)
    {
        $this->validate_rol([1]);

        $banner_object = $this->banner->get_by_id($banner_id);

        if ($banner_object) {

            if (file_exists($banner_object->resource)) {
                unlink($banner_object->resource);
            }

            $this->banner->delete($banner_id);

            $this->response->set_message(translate('message_delete_banner_ok'), ResponseMessage::SUCCESS);
            redirect("banner/index", "location", 301);
        } else {

            $this->response->set_message(translate('message_error'), ResponseMessage::SUCCESS);
            redirect("banner/index", "location", 301);
        }
    }
}
