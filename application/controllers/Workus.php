<?php

class Workus extends CI_Controller
{

    private $img_width = 600;
    private $img_height = 300;



    public function __construct()
    {
        parent::__construct();

        $this->load->model('Workus_model', 'workus');

        @session_start();
        $this->init_form_validation();
    }



    public function index()
    {

        $this->validate_rol([1]);

        $workus_object = $this->workus->get();
        
        if(!$workus_object){
            
            $workus_object = (object)[
                '_id' => null,
                'title' => '',
                'subtitle' => '',
                'description' => '',
                'resource' => null
            ];
        }

        $data['workus_object'] = $workus_object;
        $this->load_view_admin_g("workus/index", $data);
    }



    public function add()
    {
        $this->validate_rol([1]);

        $title = $this->input->post('title');
        $subtitle = $this->input->post('subtitle');
        $description = $this->input->post('description');

        $this->form_validation->set_rules('title', translate('form_label_title_lang'), 'trim|required');
        $this->form_validation->set_rules('subtitle', translate('form_label_subtitle_lang'), 'trim|required');
        $this->form_validation->set_rules('description', translate('form_label_description_lang'), 'trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            redirect("workus/index", 'location', 301);
        }

        $data = [
            'title' => $title,
            'subtitle' => $subtitle,
            'description' => $description,
            'resource' => null,
            'is_active' => 1
        ];

        $name_file = $_FILES['archivo']['name'];
        $separado = explode('.', $name_file);
        $ext = end($separado); // me quedo con la extension
        $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
        $allow_extension = in_array($ext, $allow_extension_array);

        // $_FILES['archivo']['error'] == 4 // si no se subio archivo

        if ($allow_extension) {

            $result = save_image_from_post('archivo', './uploads/workus', time(), $this->img_width, $this->img_height);
            if ($result[0]) {

                $data['resource'] = $result[1];

                $this->workus->create($data);
                $this->response->set_message(translate("message_save_form_ok"), ResponseMessage::SUCCESS);
                redirect("workus/index", "location", 301);
            } else {

                if ($_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE) $result[1] = translate('message_file_size_exceeded_php_ini');

                $this->response->set_message($result[1], ResponseMessage::ERROR);
                redirect("workus/index", "location", 301);
            }
        } else {

            $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
            redirect("workus/index/", "location", 301);
        }
    }



    public function update()
    {
        $this->validate_rol([1]);

        $title = $this->input->post('title');
        $subtitle = $this->input->post('subtitle');
        $description = $this->input->post('description');
        $workus_id = $this->input->post('workus-id');
        $old_file = $this->input->post('old-file');

        $this->form_validation->set_rules('title', translate('form_label_title_lang'), 'trim|required');
        $this->form_validation->set_rules('subtitle', translate('form_label_subtitle_lang'), 'trim|required');
        $this->form_validation->set_rules('description', translate('form_label_description_lang'), 'trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            redirect("workus/index", 'location', 301);
        }

        $data = [
            'title' => $title,
            'subtitle' => $subtitle,
            'description' => $description,
            'is_active' => 1
        ];

        $update_file = FALSE;

        $name_file = $_FILES['archivo']['name'];
        $ofile_name = explode('/', $old_file);

        if (end($ofile_name) != $name_file and $name_file != null) {
            $update_file = TRUE;
        }

        if (!$update_file) {

            $r = $this->workus->update($workus_id, $data);

            if ($r === false) {

                $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                redirect("workus/index/", "location", 301);
            } else {

                $this->response->set_message(translate("message_update_form_ok"), ResponseMessage::SUCCESS);
                redirect("workus/index/", "location", 301);
            }
        } else {

            $separado = explode('.', $name_file);
            $ext = end($separado); // me quedo con la extension
            $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
            $allow_extension = in_array($ext, $allow_extension_array);

            if ($allow_extension) {

                $result = save_image_from_post('archivo', './uploads/workus', time(), $this->img_width, $this->img_height);
                if ($result[0]) {
                    if ($old_file != null) {
                        unlink($old_file);
                    }

                    $data['resource'] = $result[1];

                    $r = $this->workus->update($workus_id, $data);
                    if ($r > 0) {
                        $this->response->set_message(translate("message_update_form_ok"), ResponseMessage::SUCCESS);
                        redirect("workus/index/", "location", 301);
                    } else {
                        $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                        redirect("workus/index/", "location", 301);
                    }
                } else {

                    if ($_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE) $result[1] = translate('message_file_size_exceeded_php_ini');

                    $this->response->set_message($result[1], ResponseMessage::ERROR);
                    redirect("workus/index/", "location", 301);
                }
            } else {

                $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
                redirect("workus/index/", "location", 301);
            }
        }
    }


    public function send_email(){

        $this->load->library('email');

        $this->load->config('email');

        $this->email->initialize();

        $this->email->from($this->email->smtp_user, 'PromoStock');
        
        //TODO: Completar mensaje de notificacion y modificar direcciones.
        $this->email->to('luciano@datalabcenter.com');

        $this->email->subject('Notificacion - Solicitud de Empleo');
        $mensaje = "completar mensaje....
        <br> 
        Muchas gracias";

        $this->email->message($mensaje);

        $this->email->send();
        //TODO: Completar mensaje y redireccion de respuesta
        $this->response->set_message('aqui mensaje...', ResponseMessage::SUCCESS);
        redirect("aqui redirect ...");
    }

}
