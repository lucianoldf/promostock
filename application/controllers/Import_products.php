<?php

/**
 * Example structure directory
 * ----- tmp import -----
 * host/../uploads/import
 * host/../uploads/import/unzip
 * host/../uploads/import/unzip/images_products
 * host/../uploads/import/unzip/images_products/images_main
 * host/../uploads/import/unzip/images_products/images_aditional
 * 
 * ----- images for products -----
 * host/../uploads/import/products
 * host/../uploads/import/products/images_main [code_product.extension]
 * --> 1.jpg
 * --> 2.jpg
 * --> [n].jpg
 * host/../uploads/import/products/images_aditional
 * --> 1-1.jpg
 * --> 1-2.jpg
 * --> 1-[n].jgp
 * --> [n]-[n].jpg
 * host/../uploads/import/products/images_small
 * 
 */


// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Import_products extends CI_Controller
{
	private const FILE_NAME_IMPORT = 'import_products.xlsx';
	private const FILE_NAME_ZIP_IMPORT = 'import_images_products.zip';

	private const WIDTH = 263;
	private const HEIGHT = 349;
	private const WIDTH_SMALL = 75;
	private const HEIGHT_SMALL = 86;

	private $path_tmp_upload_import;
	private $path_tmp_unzip_import;
	private $path_tmp_images_products;
	private $path_tmp_images_main;
	private $path_tmp_images_aditional;

	private $path_images_product;
	private $path_images_product_main;
	private $path_images_product_aditional;
	private $path_images_product_small;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Product_model', 'product');
		$this->load->model('Category_product_model', 'category');

		$this->path_tmp_upload_import = UPLOADPATH . 'import' . DIRECTORY_SEPARATOR;
		$this->path_tmp_unzip_import = $this->path_tmp_upload_import . 'unzip' . DIRECTORY_SEPARATOR;
		$this->path_tmp_images_products = $this->path_tmp_unzip_import . 'images_products' . DIRECTORY_SEPARATOR;
		$this->path_tmp_images_main = $this->path_tmp_images_products . 'images_main' . DIRECTORY_SEPARATOR;
		$this->path_tmp_images_aditional = $this->path_tmp_images_products . 'images_aditional' . DIRECTORY_SEPARATOR;

		$this->path_images_product = UPLOADPATH . 'products' . DIRECTORY_SEPARATOR;
		$this->path_images_product_main = $this->path_images_product . 'images_main' . DIRECTORY_SEPARATOR;
		$this->path_images_product_small = $this->path_images_product . 'images_small' . DIRECTORY_SEPARATOR;
		$this->path_images_product_aditional = $this->path_images_product . 'images_aditional' . DIRECTORY_SEPARATOR;
	}



	public function import_index()
	{
		$this->validate_rol([1]);

		$this->load_view_admin_g("product/import");
	}



	public function import_products()
	{
		$this->validate_rol([1]);

		$file_uploaded = $this->__upload_spreadsheet();
		if (!is_array($file_uploaded)) {

			echo json_encode([
				'status' => FALSE,
				'msg' => $file_uploaded
			]);
		} else {

			$result_import = $this->__import_spreadsheet($file_uploaded['full_path']);
			echo json_encode($result_import);
		}
	}


	public function import_images_products()
	{
		$this->validate_rol([1]);

		$file_uploaded = $this->__upload_zip();
		if (!is_array($file_uploaded)) {

			echo json_encode([
				'status' => FALSE,
				'msg' => $file_uploaded
			]);
		} else {

			$result_import = $this->__import_zip($file_uploaded['full_path']);

			// $this->__rrmdir($this->path_tmp_unzip_import);

			echo json_encode($result_import);
		}
	}



	private function __upload_spreadsheet()
	{

		$this->validate_rol([1]);

		$r = $this->__create_directory($this->path_tmp_upload_import);
		if (TRUE !== $r) {
			return $r;
		}

		$config['upload_path'] = $this->path_tmp_upload_import;
		$config['allowed_types'] = 'xlsx';
		$config['max_size'] = 2048; //Kb //TODO: tomar este valor de la constante
		$config['overwrite'] = TRUE;
		$config['file_name'] = $this::FILE_NAME_IMPORT;
		$config['file_ext_tolower'] = TRUE;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file-import')) {

			return $this->upload->display_errors();
		} else {

			return $this->upload->data();
		}
	}



	private function __upload_zip()
	{

		$this->validate_rol([1]);

		$r = $this->__create_directory($this->path_tmp_upload_import);
		if (TRUE !== $r) {
			return $r;
		}

		$config['upload_path'] = $this->path_tmp_upload_import;
		$config['allowed_types'] = 'zip';
		$config['max_size'] = MAX_FILE_IMPORT_UPLOAD * 1024; //En Kb
		$config['overwrite'] = TRUE;
		$config['file_name'] = $this::FILE_NAME_ZIP_IMPORT;
		$config['file_ext_tolower'] = TRUE;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file-zip-import')) {

			return $this->upload->display_errors();
		} else {

			return $this->upload->data();
		}
	}



	/**
	 * @return array
	 * [
	 * 	'status' => 'TRUE | FALSE',
	 * 	'msg' => message response...
	 * ]
	 */
	private function __import_zip($file_name)
	{
		log_message('debug', 'Start masive images products load');

		$this->validate_rol([1]);

		$msg_response = '';

		$return_data = [
			'status' => FALSE,
			'msg' => ''
		];

		$zip = new ZipArchive;
		$res = $zip->open($file_name);

		if ($res === FALSE) {

			$msg = "Ocurrio un error al abrir el archivo comprimido (ERROR)" . PHP_EOL;
			log_message('error', $msg);

			$return_data['msg'] = $msg;
			return $return_data;
		} else {

			$msg = "Archivo comprimido cargado (OK)" . PHP_EOL;
			log_message('debug', $msg);
			$msg_response .= $msg;

			// Delete directory extraxt if exist
			if ($this->__rrmdir($this->path_tmp_unzip_import)) {
				$msg = "Directorio temporal eliminado (OK)" . PHP_EOL;
				log_message('debug', $msg);
				$msg_response .= $msg;
			}

			// Extract zip
			$is_extracted = $zip->extractTo($this->path_tmp_unzip_import);
			$zip->close();

			if (!$is_extracted) {
				$msg = "Ocurrio un problema al extrar el archivo comprimido (ERROR)" . PHP_EOL;
				log_message('debug', $msg);
				$msg_response .= $msg;

				$return_data['msg'] = $msg_response;
				return $return_data;
			}

			$msg = "Archivo descomprimido (OK)" . PHP_EOL;
			log_message('debug', $msg);
			$msg_response .= $msg;

			$list_directoryes_verify = [
				$this->path_tmp_unzip_import,
				$this->path_tmp_images_main,
				$this->path_tmp_images_aditional
			];

			$response_checked = $this->__verify_directoryes_before_extraction($list_directoryes_verify);
			if ($response_checked !== TRUE) {
				$msg = $response_checked;
				$return_data['msg'] = $msg;
				return $return_data;
			}

			$list_directoryes_structure = [
				$this->path_tmp_images_products,		//uploads/products
				$this->path_images_product_main,		//uploads/products/images_main
				$this->path_images_product_aditional,	//uploads/products/images_aditional
				$this->path_images_product_small		//uploads/products/images_small
			];

			$response_create_directoryes = $this->__create_directoryes_for_images_products($list_directoryes_structure);
			if ($response_create_directoryes !== TRUE) {
				$msg = $response_create_directoryes;
				log_message('debug', $msg);

				$return_data['msg'] = $msg;
				return $return_data;
			}

			//TODO: tambien con directorio de images aditionales y tambien redicmencional a small
			$response_copy_images = $this->__copy_images($this->path_tmp_images_main, $this->path_images_product_main, $this::WIDTH, $this::HEIGHT, 'main');

			$msg_response .= $response_copy_images . PHP_EOL;

			$msg = "Copia de imagenes principales FINALIZADA (OK)" . PHP_EOL;
			log_message('debug', $msg);
			$msg_response .= $msg;

			$response_copy_images = $this->__copy_images($this->path_tmp_images_aditional, $this->path_images_product_aditional, $this::WIDTH, $this::HEIGHT, 'aditional');

			$msg_response .= $response_copy_images . PHP_EOL;

			$msg = "Copia de imagenes adicionales FINALIZADA (OK)" . PHP_EOL;
			log_message('debug', $msg);
			$msg_response .= $msg;

			$msg = "Proceso finalizado" . PHP_EOL;
			log_message('debug', $msg);
			$msg_response .= $msg;

			$return_data['status'] = TRUE;
			$return_data['msg'] = str_replace(PHP_EOL, '<br>', $msg_response);
			return $return_data;
		}
	}


	private function __copy_images($path_source, $path_destine, $img_width = 100, $img_height = 100, $type_resource = 'main')
	{
		// Obtenemos archivos del direcotiro
		$files = scandir($path_source);

		// $name_key_update = '';

		// switch ($type_resource) {
		// 	case 'main';
		// 		$name_key_update = 'resource_main';
		// 		break;
		// 	case 'aditional';
		// 		$name_key_update = 'resource_aditional';
		// 		break;
		// 	default:
		// 		$name_key_update = 'resource_main';
		// 		break;
		// }

		$msg_response = '';

		foreach ($files as $file) {

			if ($file == "." || $file == "..") {
				continue;
			}

			// Separamos nombre y extencion
			$exploded_file = explode('.', $file);

			if (strpos($file, '-') > 0) {

				$code_product = (explode('-', $exploded_file[0]))[0];
			} else {

				$code_product = $exploded_file[0];
			}

			$ext_file = strtolower(end($exploded_file));

			// Verificamos extencion valida
			$allow_extension_array = ["jpg", "jpeg", "png", "bmp", "gif"];
			$is_allow_extension = in_array($ext_file, $allow_extension_array);

			if (!$is_allow_extension) {
				$msg = "Archivo " . $file . " no es una extencion permitida" . PHP_EOL;
				log_message('debug', $msg);
				$msg_response .= $msg;
				continue;
			}

			// Verificamos si exite un producto con el codigo indicado por el nombre del archivo

			if ($type_resource === 'main') {

				$current_product = $this->product->get_code_by_code($code_product);
			} else if ($type_resource === 'aditional') {

				$current_product = $this->product->get_resources_by_code($code_product);
			}

			if (!$current_product) {
				$msg = "No existe un producto con codigo: " . $code_product . " para el archivo '<em>" . $file . "</em>" . PHP_EOL;
				log_message('debug', $msg);
				$msg_response .= $msg;
				continue;
			}

			// Copiamos y redimencionamos la imagen
			$is_resized = image_resize($path_source . '/' . $file, $path_destine . '/' . $file, $img_width, $img_height);

			if (!$is_resized) {
				$msg = "Guardado de imagen para producto: " . $code_product . " (ERROR)" . PHP_EOL;
				log_message('debug', $msg);
				$msg_response .= $msg;
				continue;
			}

			$msg = "Imagen de producto : " . $code_product . " ($file) guardada (OK)";
			log_message('debug', $msg);
			$msg_response .= $msg;

			$path_host_save = '.' . str_replace($_SERVER['DOCUMENT_ROOT'], '', $path_destine . $file);

			if ($type_resource === 'main') {

				$data_save_db = [
					'resource_main' => $path_host_save
				];
			} else if ($type_resource === 'aditional') {

				$update_resource = [];
				if (is_array($current_product['resource_aditional'])) {
					$update_resource = $current_product['resource_aditional'];
				}

				array_push($update_resource, $path_host_save);


				$data_save_db = [
					'resource_aditional' => $update_resource
				];
			}



			$is_update = $this->product->update_by_code($code_product, $data_save_db);

			if ($is_update) {
				$msg = " - Actualizado (OK)" . PHP_EOL;
				log_message('debug', $msg);
				$msg_response .= $msg;
			} else {
				$msg = " - Actualizado (ERROR)" . PHP_EOL;
				log_message('debug', $msg);
				$msg_response .= $msg;
			}
		}

		return $msg_response;
	}


	private function __verify_directoryes_before_extraction($list_path_verify)
	{
		foreach ($list_path_verify as $path) {

			$r = $this->__verify_directory_exist($path);
			if ($r !== TRUE) {
				return $r;
			}
		}

		return TRUE;
	}


	private function __verify_directory_exist($path_directory)
	{

		$name =  explode(DIRECTORY_SEPARATOR, $path_directory);
		if (!is_dir($path_directory)) {
			$msg = "No existe el directorio " . $name[count($name) - 2] . PHP_EOL;
			log_message('debug', $msg);
			return $msg;
		}

		return TRUE;
	}


	private function __create_directory($path_directory)
	{

		if (!is_dir($path_directory)) {

			if (!mkdir($path_directory, DIR_READ_MODE, TRUE)) {
				$msg = "Creacion de directorio " . $path_directory . " (ERROR)" . PHP_EOL;
				log_message('debug', $msg);
				return $msg;
			}
		}
		return TRUE;
	}


	private function __create_directoryes_for_images_products($list_directoryes_structure)
	{
		foreach ($list_directoryes_structure as $directory) {
			$r = $this->__create_directory($directory);
			if (TRUE !== $r) {
				return $r;
			}
		}

		return TRUE;
	}

	/**
	 * @return array
	 * [
	 * 	'status' => 'TRUE | FALSE',
	 * 	'msg' => message response...
	 * ]
	 */
	private function __import_spreadsheet($path_file_import)
	{
		log_message('debug', 'Start masive products load');

		$this->validate_rol([1]);

		require 'vendor/autoload.php';

		$msg_response = '';

		$return_data = [
			'status' => FALSE,
			'msg' => ''
		];

		try {

			$input_file_type = IOFactory::identify($path_file_import);

			$msg_response .= 'Archivo leído: "<em>' . pathinfo($path_file_import, PATHINFO_BASENAME) . '</em>"' . PHP_EOL;
			log_message('debug', $msg_response);

			$reader = IOFactory::createReader($input_file_type);
			$spreadsheet = $reader->load($path_file_import);

			$sheet_data = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
		} catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {

			log_message('error', 'loading file:  ' . pathinfo($path_file_import, PATHINFO_BASENAME)) . PHP_EOL;
			log_message('error', $e->getMessage());

			$msg_response .= "Error al leer el arhcivo: " . $e->getMessage() . PHP_EOL;
			$return_data['msg'] = $msg_response;
			return $return_data;
		}

		// Initilize counter
		$count_save = 0;
		$count_update = 0;
		$count_error = 0;
		$count_total = 0;
		$count_category_invalid = 0;

		foreach ($sheet_data as $idx => $row) {

			// Saltamos encabezados
			if ($idx === 1) {
				continue;
			}

			// Comprovamos que no existan celdas sin datos
			$flag_nex_row = FALSE;
			foreach ($row as $key => $cell) {
				if (trim($cell) === false || $cell === NULL || trim($cell) === '') {
					$msg = "No pueden existir celdas sin datos ($idx - $key) (ERROR)" . PHP_EOL;
					log_message('debug', $msg);
					$msg_response .= $msg;
					$count_error++;
					$flag_nex_row = TRUE;
					break;
				}
			}

			if ($flag_nex_row) {
				continue;
			}


			$count_total++;
			$row_data_current_product = [
				'code' => (string) $row['A'],
				'name' => (string) trim($row['B']),
				'category' => (string) trim($row['D']),
				'description' => (string) trim($row['C']),
				'price_final' => (float) $row['E']
			];

			$current_product_code = (string) $row['A'];

			$data_save_db = [
				'code' => $row_data_current_product['code'],
				'name' => $row_data_current_product['name'],
				'category' => $row_data_current_product['category'],
				'description' => $row_data_current_product['description'],
				'price_final' => $row_data_current_product['price_final'],
				'resource_main' => '',
				'resource_aditional' => array(),
				'aditional_information' => array(),
				'is_active' => 1
			];

			$category_exist = FALSE;
			$all_categoryes = $this->category->get_all();
			foreach ($all_categoryes as $category) {
				if ($category->_id == $row_data_current_product['category']) {
					$category_exist = TRUE;
					break;
				}
			}

			// $category_exist = $this->category->get_by_id($row_data_current_product['category']);
			if (!$category_exist) {
				$msg = "La categoría [{$row_data_current_product['category']}] - Producto: [{$row_data_current_product['code']}]  no existe (ERROR)" . PHP_EOL;
				log_message('debug', $msg);
				$msg_response .= $msg;
				$count_category_invalid++;
				continue;
			}

			$code_exist = $this->product->get_code_by_code($row_data_current_product['code']);

			// Actualizamos si existe
			if ($code_exist) {

				$msg = "El producto [{$row_data_current_product['code']}] ya existe - ";
				log_message('debug', $msg);
				$msg_response .= $msg;

				$is_update = $this->product->update($code_exist->_id, $data_save_db);

				if ($is_update) {

					$msg = "Actualización (OK)" . PHP_EOL;
					log_message('debug', $msg);
					$msg_response .= $msg;
					$count_update++;
				} else {

					$msg = "Actualización (ERROR)" . PHP_EOL;
					log_message('debug', $msg);
					$msg_response .= $msg;
					$count_error++;
				}

				// Cargamos nuevo producto
			} else {

				$msg = "Nuevo producto [{$row_data_current_product['code']}] - ";
				log_message('debug', $msg);
				$msg_response .= $msg;

				$is_save = $this->product->create($data_save_db);

				if ($is_save) {

					$msg = "Nuevo producto cargado [{$row_data_current_product['code']}] (OK)" . PHP_EOL;
					log_message('debug', $msg);
					$msg_response .= $msg;
					$count_save++;
				} else {

					$msg = "Nuevo producto [{$row_data_current_product['code']}] (ERROR)" . PHP_EOL;
					log_message('debug', $msg);
					$msg_response .= $msg;
					$count_error++;
				}
			}
		}

		$flg_problem = FALSE;
		if($count_error > 0 || $count_category_invalid > 0){
			$flg_problem = TRUE;
		}

		$msg = "Resumen: ";
		$msg .= "Total de productos procesados: $count_total" . PHP_EOL;
		$msg .= "Total de productos nuevos cargados: $count_save" . PHP_EOL;
		$msg .= "Total de productos actualizados: $count_update" . PHP_EOL;
		$msg .= "Total de productos con categoría inexistente: $count_category_invalid" . PHP_EOL;
		$msg .= "Total de errores: $count_error" . PHP_EOL;
		$msg .= "Proceso finalizado";

		if($flg_problem){
			$msg .= " - Con probelmas (ERROR). Por favor revise el detalle de importacion.";
		}else{
			$msg .= " - (OK)";
		}

		log_message('debug', $msg);
		$msg_response .=  $msg;

		$return_data['status'] = TRUE;
		$return_data['msg'] = str_replace(PHP_EOL, '<br>', $msg_response);

		return $return_data;
	}


	//TODO: implementar en helper
	private function __rrmdir($dir)
	{
		if (is_dir($dir)) {
			$objects = scandir($dir);

			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . DIRECTORY_SEPARATOR . $object))
						$this->__rrmdir($dir . DIRECTORY_SEPARATOR . $object);
					else
						unlink($dir . DIRECTORY_SEPARATOR . $object);
				}
			}
			rmdir($dir);
			return TRUE;
		}
		return FALSE;
	}
}
