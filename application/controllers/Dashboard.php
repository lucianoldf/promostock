<?php
// require(APPPATH . "libraries/facebook/src/facebook.php");

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('User_model', 'user');
        
        // $this->load->library(array('session'));
        // $this->load->helper("mabuya");

        @session_start();
        // $this->load_language();
        $this->init_form_validation();

        $this->validate_rol([1]);
        
    }

    public function index()
    {

        $this->validate_rol([1]);
        
        $this->load_view_admin_g('dashboard/index');
    }
}
