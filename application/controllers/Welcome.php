<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see https://codeigniter.com/user_guide/general/urls.html
	*/

	public function __construct(){
		parent::__construct();

		// echo "Hola, soy el constructor de Welcome!" . '<br>';
		// echo "El entorno actual es: " . ENVIRONMENT . '<br>';
		// echo "\tConfiguracion de session: " . $this->config->item('sess_save_path') . '<br>';
		// die();
	}


	public function index()
	{
		// $datajson = [
		// 	"destino" => [
		// 		"identificacionD"=> "1715952113",
		// 		"ciudadD"=> "201001001001",
		// 		"nombreD"=> "LUIS MONTUFAR",
		// 		"direccion"=> "El Inca",
		// 		"referencia"=> "El Bosque",
		// 		"numeroCasa"=> "",
		// 		"postal"=> "",
		// 		"telefono"=> "0990566211",
		// 		"celular"=> "0990566211"
		// 	],
		// 	"numeroGuia"=> "",
		// 	"tipoServicio"=> "201202002002013",
		// 	"noPiezas"=> 1,
		// 	"peso"=> 1.3,
		// 	"valorDeclarado"=> 15.3,
		// 	"contiene"=> "DECODIFICADOR",
		// 	"tamanio"=> "",
		// 	"cod"=> false,
		// 	"costoflete"=> 0,
		// 	"costoproducto"=> 0,
		// 	"tipocobro"=> 0
		// 	];

		// 	$strjson = '{
		// 		"destino": {
		// 			"identificacionD": "1715952113",
		// 			"ciudadD": "201001001001",
		// 			"nombreD": "LUIS MONTUFAR",
		// 			"direccion": "El Inca",
		// 			"referencia": "El Bosque",
		// 			"numeroCasa": "",
		// 			"postal": "",
		// 			"telefono": "0990566211",
		// 			"celular": "0990566211"
		// 		},
		// 		"numeroGuia": "",
		// 		"tipoServicio": "201202002002013",
		// 		"noPiezas": 1,
		// 		"peso": 1.3,
		// 		"valorDeclarado": 15.3,
		// 		"contiene": "DECODIFICADOR",
		// 		"tamanio": "",
		// 		"cod": false,
		// 		"costoflete": 0,
		// 		"costoproducto": 0,
		// 		"tipocobro": 0
		// 	}';

		// // echo $strjson;
		// // die();

		// //$this->load->view('test_images');
		// $url = "http://api.laarcourier.com:9027/guias";
		
		// $headers = array(
		// 	"POST: http://api.laarcourier.com:9027/guias",
		// 	"Content-type: application/json",
		// 	"Accept: application/json",
		// 	// "Cache-Control: no-cache",
		// 	// "Pragma: no-cache",
		// 	// "SOAPAction: \"run\"",
		// 	// "Content-length: ".strlen($xml_data),
		// 	"Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjMzMzciLCJyb2xlIjoiMSIsImFjdG9ydCI6IjQ3OTczIiwibmJmIjoxNTc1OTk2OTc0LCJleHAiOjE1NzYwMDQxNzQsImlhdCI6MTU3NTk5Njk3NCwiaXNzIjoiYXBpLmxhYXJjb3VyaWVyLmNvbTo5MDI4IiwiYXVkIjoiYXBpLmxhYXJjb3VyaWVyLmNvbTo5MDI4In0.8ZfKvn3Pp-ekOQdV2OilSetit_P3H8xa4rcOIMHv31M"
		// );

		// $handle = curl_init();
 
		// curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);

		// curl_setopt($handle, CURLOPT_HEADER, true);

		// // Set the url
		// curl_setopt($handle, CURLOPT_URL, $url);
		
		// // Set the result output to be a string.
		// curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

		// curl_setopt($handle, CURLOPT_POST, 1);

		// curl_setopt($handle, CURLOPT_POSTFIELDS, $strjson);

 
		// $output = curl_exec($handle);
 
		// curl_close($handle);
 
		// json_decode($output);

		// die();

	}
}
