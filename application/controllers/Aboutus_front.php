<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aboutus_front extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('About_model', 'about');

		@session_start();
	}


	public function index()
	{

		$about_object = $this->about->get();

		$data['about_object'] = $about_object;

		$this->load_view_front('aboutus', $data);
	}
}
