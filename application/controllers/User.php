<?php

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        @session_start();

        $this->load->model('User_model', 'user');
        $this->load->model('Role_model', 'role');

        $this->init_form_validation();

        $this->response->set_error_delimiters('<div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>');
        $this->response->set_success_delimiters('<div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        ', '</div>');
    }



    public function index()
    {
        $this->validate_rol([1]);

        $all_users = $this->user->get_all();

        $data['all_users'] = $all_users;

        $this->load_view_admin_g("user/index", $data);
    }



    public function add_index()
    {
        $this->validate_rol([1]);

        $data['all_roles'] = $this->role->get_all();
        $this->load_view_admin_g('user/add', $data);
    }



    function update_index($user_id = 0)
    {
        $this->validate_rol([1]);

        $user_object = $this->user->get_by_id($user_id);

        $data['all_roles'] = $this->role->get_all();

        if ($user_object) {

            $data['user_object'] = $user_object;

            $this->load_view_admin_g('user/update', $data);
        } else {

            show_404();
        }
    }



    public function profile_index()
    {
        $this->validate_rol([1]);

        $user_id = $this->session->userdata('user_id');

        $user_object = $this->user->get_by_id($user_id);

        if ($user_object) {

            $data['user_object'] = $user_object;
            $this->load_view_admin_g('user/profile', $data);
        } else {
            show_404();
        }
    }



    public function credenciales_index()
    {
        $this->validate_rol([1]);

        $user_id = $this->session->userdata('user_id');

        $user_object = $this->user->get_by_id($user_id);

        if ($user_object) {

            $data['user_object'] = $user_object;
            $this->load_view_admin_g('user/credenciales', $data);
        } else {
            show_404();
        }
    }




    public function add()
    {
        $this->validate_rol([1]);

        $name = $this->input->post('fullname');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $phone = $this->input->post('phone');
        $role = $this->input->post('role');

        $this->form_validation->set_rules('fullname', translate('name_label_lang'), 'trim|required');

        $this->form_validation->set_rules('phone', translate('phone_label_lang'), 'trim|required');

        $this->form_validation->set_rules(
            'email',
            translate('email_label_lang'),
            "trim|required|callback_validate_form_email"
        );

        $this->form_validation->set_rules('password', translate('pwd_label_lang'), 'trim|required|min_length[8]');

        $this->form_validation->set_rules(
            'repeat_password',
            translate('repeat_pwd_label_lang'),
            'required|matches[password]',
            ['matches' => translate('matches'), 'required' => translate('required')]
        );

        $this->form_validation->set_rules('role', translate('role_label_lang'), 'required');

        if ($this->form_validation->run() == FALSE) {

            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            $data['all_roles'] = $this->role->get_all();
            $this->load_view_admin_g('user/add', $data);
            // redirect("user/add_index");
        } else {

            $data_user = [
                'name' => (string) $name,
                'email' => (string) $email,
                'password' => (string) md5($password),
                'phone' => (string) $phone,
                'role_id' => (int) $role,
            ];

            $this->user->create($data_user);
            $this->response->set_message(translate('message_save_user_ok'), ResponseMessage::SUCCESS);
            redirect("user/index");
        }
    }



    public function update()
    {
        $this->validate_rol([1]);

        $name = $this->input->post('fullname');
        $role = $this->input->post('role');
        $email = $this->input->post('email');
        $email_old = $this->input->post('email_old');
        $user_id = $this->input->post('user_id');

        //establecer reglas de validacion
        $this->form_validation->set_rules('fullname', translate('fullname_lang'), 'trim|required');

        if ($email != $email_old) {
            $this->form_validation->set_rules(
                'email',
                translate('email_label_lang'),
                "trim|required|callback_validate_form_email"
            );
        }

        if ($this->form_validation->run() == FALSE) {

            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            redirect("user/update_index/" . $user_id);
        } else {

            $data_user = [
                'name' => $name,
                'role_id' => $role
            ];

            if ($email != $email_old) $data_user['email'] = $email;

            $this->user->update($user_id, $data_user);
            $this->response->set_message(translate('message_update_user_ok'), ResponseMessage::SUCCESS);
            redirect("user/index");
        }
    }



    public function delete($user_id = 0)
    {
        $this->validate_rol([1]);

        $response = $this->user->delete($user_id);

        if ($response) {

            $this->response->set_message(translate('message_delete_user_ok'), ResponseMessage::SUCCESS);
            redirect("user/index");
        } else {

            $this->response->set_message(translate('message_error'), ResponseMessage::ERROR);
            redirect("user/index");
        }
    }



    public function edit_credential()
    {
        $this->validate_rol([1]);

        $email = $this->input->post('email');
        $email_old = $this->input->post('email_old');
        $current_pwd = $this->input->post('current_pwd');
        $new_pwd = $this->input->post('new_pwd');
        $new_repeat_pwd = $this->input->post('new_repeat_pwd');
        $user_id = $this->input->post('user_id');

        if ($email != $email_old) {
            $this->form_validation->set_rules(
                'email',
                translate('email_label_lang'),
                "trim|required|callback_validate_form_email"
            );
        }

        $this->form_validation->set_rules(
            'current_pwd',
            translate('current_pwd_label_lang'),
            "callback_validate_pwd[$user_id]"
        );

        if($new_pwd != null OR $new_repeat_pwd != null){
            $this->form_validation->set_rules('new_pwd', translate('new_pwd_label_lang'), 'trim|min_length[8]|required');

            $this->form_validation->set_rules(
                'new_repeat_pwd',
                translate('repeat_new_pwd_label_lang'),
                'trim|required|matches[new_pwd]',
                ['matches' => translate('matches'), 'required' => translate('required')]
            );
        }

        if ($this->form_validation->run() == FALSE) {

            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            $this->load_view_admin_g('user/credenciales');
        } else {
            $data_user = [];
            if ($new_pwd != null) $data_user['password'] = md5($new_pwd);
            if ($email != $email_old) $data_user['email'] = $email;

            if(count($data_user) == 0){
                redirect("user/index");
            }
            if( $this->user->update($user_id, $data_user) ){
                
                // $this->response->set_message(translate('message_update_user_ok'), ResponseMessage::SUCCESS);
                // redirect("user/index");
                $this->log_out();
                redirect('login');
            }

            
        }
    }



    public function edit_profile()
    {
        $this->validate_rol([1]);

        $name = $this->input->post('fullname');
        $phone = $this->input->post('phone');
        $email = $this->input->post('email');
        $email_old = $this->input->post('email_old');
        $user_id = $this->input->post('user_id');

        //establecer reglas de validacion
        $this->form_validation->set_rules('fullname', translate('fullname_lang'), 'trim|required');
        $this->form_validation->set_rules('phone', translate('phone_lang'), 'trim|required');

        if ($email != $email_old) {
            $this->form_validation->set_rules(
                'email',
                translate('email_label_lang'),
                "trim|required|callback_validate_form_email"
            );
        }

        if ($this->form_validation->run() == FALSE) {

            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            redirect("user/profile_index/");
        } else {

            $name_file = $_FILES['archivo']['name'];
            $error = $_FILES['archivo']['error'];

            $separado = explode('.', $name_file);
            $ext = end($separado); // me quedo con la extension
            $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
            $allow_extension = in_array($ext, $allow_extension_array);

            if ($allow_extension || $error == 4) {
                if ($error == 0) {

                    $user_object = $this->user->get_by_id($user_id);
                    if (file_exists($user_object->photo))
                        unlink($user_object->photo);

                    $result = save_image_from_post('archivo', './uploads/user', time(), 350, 350);

                    if ($result[0]) {

                        $data_update = [
                            "name" => $name,
                            "phone" => $phone,
                            "photo" => $result[1]
                        ];

                        if ($email != $email_old) $data_update['email'] = $email;

                        $this->user->update($user_id, $data_update);
                        $this->session->set_userdata("photo", $result[1]);
                        $this->response->set_message(translate('message_update_user_ok'), ResponseMessage::SUCCESS);
                        redirect("user/profile_index");
                    } else {

                        $this->response->set_message($result[1], ResponseMessage::ERROR);
                        redirect("user/profile_index/");
                    }
                } else {

                    $data_creation = [
                        "name" => $name,
                        "phone" => $phone
                    ];

                    if ($email != $email_old) $data_update['email'] = $email;

                    $this->user->update($user_id, $data_creation);
                    $this->response->set_message(translate('message_update_user_ok'), ResponseMessage::SUCCESS);
                    redirect("user/profile_index");
                }
            } else {

                $this->response->set_message(translate('message_file_tipe_not_accept'), ResponseMessage::ERROR);
                redirect("user/profile_index/");
            }
        }
    }



    // Callback validate form - email
    public function validate_form_email($email)
    {
        // True si el email ya existe
        if ($this->user->get_by_email($email)) {

            $this->form_validation->set_message('validate_form_email', translate('email_not_unique_lang'));
            return FALSE;
        } else {
            return TRUE;
        }
    }



    public function validate_pwd($current_pwd, $user_id)
    {
        if (!$this->user->validate_password($user_id, $current_pwd)) {
            $this->form_validation->set_message('validate_pwd', translate('password_incorrect_lang'));
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
