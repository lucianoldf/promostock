<?php

class Product extends CI_Controller
{

    private const WIDTH = 886;
    private const HEIGHT = 960;
    private const WIDTH_SMALL = 75;
    private const HEIGHT_SMALL = 86;
    private const WIDTH_CATALOG = 250;
    private const HEIGHT_CATALOG = 250;
    private const DEFAULT_STOCK_QUANTITY = 1;
    private $data;

    private $path_images_product;
    private $path_images_product_main;
    private $path_images_product_aditional;
    private $path_images_product_small;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Product_model', 'product');

        $this->data['img_width'] = $this::WIDTH;
        $this->data['img_height'] = $this::HEIGHT;

        $this->path_images_product = UPLOADPATH . 'products' . DIRECTORY_SEPARATOR;
        $this->path_images_product_main = $this->path_images_product . 'images_main';
        $this->path_images_product_small = $this->path_images_product . 'images_small';
        $this->path_images_product_aditional = $this->path_images_product . 'images_aditional';

        @session_start();
        $this->init_form_validation();
    }


    public function index()
    {

        $this->validate_rol([1]);

        $products = $this->product->get_all();
        $this->data['products'] = $products;
        $this->load_view_admin_g("product/index", $this->data);
    }



    public function add_index()
    {
        $this->validate_rol([1]);

        $this->load->model('Category_product_model', 'category');

        $all_category_prod = $this->category->get_all();
        $this->data['categorys_product'] = $all_category_prod;

        $next_code = $this->product->get_next_code();
        $this->data['next_code'] = $next_code;

        $this->data['default_stock_quantity'] = $this::DEFAULT_STOCK_QUANTITY;

        $this->load_view_admin_g('product/add', $this->data);
    }



    public function add()
    {
        $this->validate_rol([1]);

        $code = (int) $this->input->post('code');
        $name = (string) $this->input->post('name');
        $category_id = (string) $this->input->post('category');
        $description = (string) $this->input->post('description');
        $price_final = (float) $this->input->post('price-final');
        $starts = (int) ($this->input->post('starts') != null) ? $this->input->post('starts') : 0;
        $is_deal = (int) $this->input->post('is_deal');
        $is_show_deal_banner = (int) $this->input->post('is_show_deal_banner');
        $stock_quantity = (int) $this->input->post('stock_quantity');

        $this->form_validation->set_rules('name', translate('product_label_name_lang'), 'trim|required');
        $this->form_validation->set_rules('description', translate('product_label_description_lang'), 'trim|required');

        if ($this->form_validation->run() == FALSE) {

            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            redirect("product/add_index", "location", 301);
        }

        $data_save_db = [
            'code' => (string) $code,
            'name' => (string) $name,
            'category' => (string) $category_id,
            'description' => (string) $description,
            'price_final' => (float) $price_final,
            'resource_main' => '',
            'resource_small' => '',
            'resource_catalog' => '',
            'aditional_information' => null,
            'is_active' => 1,
            'stock_quantity' => (int) $stock_quantity,
            'is_deal' => (int) $is_deal,
            'show_deal_banner' => (int) $is_show_deal_banner,
            'sum_starts' => (int) $starts,
            'starts_average' => (int) $starts,
            'total_score' => ($starts != 0) ? 1 : 0,
            'views' => 0,
            'created_at' => time(),
            'updated_at' => time(),
        ];

        $images_product = $this->__save_product_images($_FILES['archivo']['name'], $code . "_" . time());

        if (is_string($images_product)) {
            $this->response->set_message($images_product, ResponseMessage::ERROR);
            redirect("product/index", "location", 301);
        }

        if (is_array($images_product)) {
            $data_save_db['resource_main'] = $images_product['resource_main'];
            $data_save_db['resource_small'] = $images_product['resource_small'];
            $data_save_db['resource_catalog'] = $images_product['resource_catalog'];

            $is_create = $this->product->create($data_save_db);

            if ($is_create) {

                $this->response->set_message(translate("message_save_product_ok"), ResponseMessage::SUCCESS);
                redirect("product/index", "location", 301);
            } else {

                $this->response->set_message("Ocurrio un error al guardar el producto", ResponseMessage::ERROR);
                redirect("product/add_index", "location", 301);
            }
        }
    }



    function update_index($product_id = 0)
    {
        $this->validate_rol([1]);

        $this->load->model('Category_product_model', 'category');

        $all_category_prod = $this->category->get_all();
        $this->data['categorys_product'] = $all_category_prod;

        $product_object = $this->product->get_by_id($product_id);

        if (isset($product_object->stock_quantity) == FALSE) {
            $product_object->stock_quantity = $this::DEFAULT_STOCK_QUANTITY;
        }

        if ($product_object) {
            $this->data['product_object'] = $product_object;
            $this->load_view_admin_g('product/update', $this->data);
        } else {

            show_404();
        }
    }



    public function update_aditional_images_index($product_id = 0)
    {
        $this->validate_rol([1]);

        $product_object = $this->product->get_by_id($product_id);

        if ($product_object) {

            if (!property_exists($product_object, 'aditional_information') or $product_object->aditional_information == NULL) {
                $product_object->aditional_information = array_to_object([
                    'images' => [
                        'resource_aditional_1' => [
                            'path' => '',
                            'show_deal_banner' => FALSE,
                        ],
                        'resource_aditional_2' => [
                            'path' => '',
                            'show_deal_banner' => FALSE,
                        ],
                        'resource_aditional_3' => [
                            'path' => '',
                            'show_deal_banner' => FALSE,
                        ],
                        'resource_aditional_4' => [
                            'path' => '',
                            'show_deal_banner' => FALSE,
                        ],
                    ],
                ]);
            } else {
                foreach (object_to_array($product_object->aditional_information->images) as $key => $resource) {

                    if (!empty($resource['path'])) {

                        // Get small resource path if exist
                        $small_resource = $this->__get_small_image_if_exist($resource['path'], 'images_aditional');

                        if (!empty($small_resource)) {
                            // Set key small for load front
                            $product_object->aditional_information->images->$key->small = $small_resource;
                        } else {

                            // If not exist set normal resource for load front
                            $product_object->aditional_information->images->$key->small = $resource['path'];
                        }
                    }
                }
            }


            $this->data['product_object'] = $product_object;
            $this->load_view_admin_g('product/update_images_aditional', $this->data);
        } else {

            show_404();
        }
    }


    public function delete_aditional_image()
    {
        $this->validate_rol([1]);

        $product_id = $this->input->post('product_id');
        $path_image = $this->input->post('path_image');
        $key_resource = $this->input->post('key_resource');

        if (!$product_id) {
            $this->response->set_message("Producto inexistente", ResponseMessage::ERROR);

            echo site_url('product/index');
            return 0;
            //redirect("product/index", "location", 301);
        }

        if (!$key_resource) {
            $this->response->set_message("key_resource not define", ResponseMessage::ERROR);

            echo site_url('product/index');
            return 0;
            //redirect("product/index", "location", 301);
        }

        if (!file_exists($path_image)) {
            $this->response->set_message("Archivo no existe", ResponseMessage::ERROR);

            echo site_url('product/update_aditional_images_index/' . $product_id);
            return 0;
            //redirect("product/update_aditional_images_index/" . $product_id, "location", 301);
        }

        unlink($path_image);


        $product = $this->product->get_by_id($product_id);

        $data_save_db['aditional_information'] = object_to_array($product->aditional_information);

        $data_save_db['aditional_information']['images'][$key_resource]['path'] = '';
        $data_save_db['aditional_information']['images'][$key_resource]['show_deal_banner'] = FALSE;

        $this->product->update($product_id, $data_save_db);

        $small_resource = $this->__get_small_image_if_exist($path_image, 'images_aditional');

        if (file_exists($small_resource)) {
            unlink($small_resource);
        }

        $this->response->set_message(translate('message_update_product_ok'), ResponseMessage::SUCCESS);

        echo site_url('product/update_aditional_images_index/' . $product_id);
        return 0;
        //redirect("product/update_aditional_images_index/" . $product_id, "location", 301);

    }

    public function update_aditional_images()
    {
        $this->validate_rol([1]);

        $product_id = $this->input->post('product-id');
        $code = (int) $this->input->post('code');

        $old_images['resource_aditional_1'] = [$this->input->post('old_aditional_image_1'), FALSE];
        $old_images['resource_aditional_2'] = [$this->input->post('old_aditional_image_2'), FALSE];
        $old_images['resource_aditional_3'] = [$this->input->post('old_aditional_image_3'), FALSE];
        $old_images['resource_aditional_4'] = [$this->input->post('old_aditional_image_4'), FALSE];

        $data_save_db = [
            // 'aditional_information' => [
            //     'images' => [
            //         'resource_aditional_1' => [
            //             'path' => '',
            //             'show_deal_banner' => ($this->input->post('show_deal_banner_1') == 1) ? TRUE : FALSE,
            //         ],
            //         'resource_aditional_2' => [
            //             'path' => '',
            //             'show_deal_banner' => ($this->input->post('show_deal_banner_2') == 1) ? TRUE : FALSE,
            //         ],
            //         'resource_aditional_3' => [
            //             'path' => '',
            //             'show_deal_banner' => ($this->input->post('show_deal_banner_3') == 1) ? TRUE : FALSE,
            //         ],
            //         'resource_aditional_4' => [
            //             'path' => '',
            //             'show_deal_banner' => ($this->input->post('show_deal_banner_4') == 1) ? TRUE : FALSE,
            //         ],
            //     ],
            // ],
            'updated_at' => time(),
        ];

        $new_images['resource_aditional_1'] = $_FILES['resource_aditional_1']['name'];
        $new_images['resource_aditional_2'] = $_FILES['resource_aditional_2']['name'];
        $new_images['resource_aditional_3'] = $_FILES['resource_aditional_3']['name'];
        $new_images['resource_aditional_4'] = $_FILES['resource_aditional_4']['name'];


        $arr_response_error = [];
        $arr_response_success = [];

        foreach ($old_images as $key => $image) {
            $current_old_name = explode('/', $image[0]);
            $current_old_name = end($current_old_name);

            if ($current_old_name != $new_images[$key] && $new_images[$key] != NULL) {
                $old_images[$key][1] = TRUE;
            }
        }

        $product = $this->product->get_by_id($product_id);
        $data_save_db['aditional_information'] = object_to_array($product->aditional_information);

        foreach ($old_images as $key => $image) {

            if ($image[1]) {

                $images_product = $this->__save_product_images($new_images[$key], $code . "_" . $key . "_" . time(), TRUE, $key);

                // Error in save image file
                if (is_string($images_product)) {
                    $arr_response_error[$key] = $images_product;

                    // $this->response->set_message($images_product, ResponseMessage::ERROR);
                    // redirect("product/index", "location", 301);
                }

                // Save path new image in db
                if (is_array($images_product)) {

                    $data_save_db['aditional_information']['images'][$key]['path'] = $images_product['resource_aditional'];
                    $data_save_db['aditional_information']['images'][$key]['show_deal_banner'] = ($this->input->post('show_deal_banner_' . $key) == 1) ? TRUE : FALSE;

                    // Delete old file if exist
                    if (file_exists($image[0])) {
                        unlink($image[0]);

                        $small_resource = $this->__get_small_image_if_exist($image[0], 'images_aditional');
                        if (file_exists($small_resource)) {
                            unlink($small_resource);
                        }
                    }

                    $is_update = $this->product->update($product_id, $data_save_db);

                    if ($is_update) {

                        $arr_response_success[$key] = TRUE;
                    } else {

                        $arr_response_success[$key] = FALSE;
                        $arr_response_error[$key] = $new_images[$key];
                    }
                }
            } else {

                if (!empty($data_save_db['aditional_information']['images'][$key]['path'])) {
                    $data_save_db['aditional_information']['images'][$key]['show_deal_banner'] = ($this->input->post('show_deal_banner_' . $key) == 1) ? TRUE : FALSE;

                    $is_update = $this->product->update($product_id, $data_save_db);
                }
            }
        }

        $status_global_operation = TRUE;

        foreach ($arr_response_success as $key => $status) {
            $msg_error = "";
            if ($status == FALSE or $arr_response_error[$key] != NULL) {
                $status_global_operation = FALSE;
                $msg_error .= '<p>Error al guardar el archivo (' . $arr_response_success[$key];

                if ($arr_response_error[$key] != NULL) {
                    $msg_error .= ' - ' . $arr_response_error[$key] . ')</p>';
                } else {
                    $msg_error .= ')</p>';
                }
            }
        }

        if (!$status_global_operation) {
            $this->response->set_message($msg_error, ResponseMessage::ERROR);
            redirect("product/add_index", "location", 301);
        } else {
            $this->response->set_message(translate("message_save_product_ok"), ResponseMessage::SUCCESS);
            redirect("product/index", "location", 301);
        }
    }

    private function __get_small_image_if_exist($path_source, $resource_type = 'images_main')
    {
        $file_name = explode('/', $path_source);
        $file_name = end($file_name);

        // Add sufix _small
        $new_file_name = explode('.', $file_name);
        $new_file_name = $new_file_name[0] . '_small.' . $new_file_name[1];

        // Replace directory resource
        $new_path = str_replace($resource_type, 'images_small', $path_source);

        // Replace name file
        $new_path = str_replace($file_name, $new_file_name, $new_path);

        if (file_exists($new_path)) {

            return $new_path;
        } else {

            return FALSE;
        }
    }

    public function update_status($status, $id)
    {
        $this->validate_rol([1]);

        $data_save_db = [
            'is_active' => (int) $status,
        ];

        $r = $this->product->update($id, $data_save_db);
        if ($r === false) {

            $this->response->set_message(translate("message_error"), ResponseMessage::SUCCESS);
            redirect("product/index/", "location", 301);
        } else {

            $this->response->set_message(translate("message_update_product_ok"), ResponseMessage::SUCCESS);
            redirect("product/index/", "location", 301);
        }
    }



    public function update()
    {
        $this->validate_rol([1]);

        $code = (int) $this->input->post('code');
        $name = (string) $this->input->post('name');
        $category_id = (string) $this->input->post('category');
        $description = (string) $this->input->post('description');
        $price_final = (float) $this->input->post('price-final');
        $is_deal = (int) $this->input->post('is_deal');
        $is_show_deal_banner = (int) $this->input->post('is_show_deal_banner');
        $stock_quantity = (int) $this->input->post('stock_quantity');
        //$starts = (int) ($this->input->post('starts') != null) ? $this->input->post('starts') : 0;

        $product_id = $this->input->post('product-id');
        $old_file = $this->input->post('old-file');

        $this->form_validation->set_rules('name', translate('product_label_name_lang'), 'trim|required');
        $this->form_validation->set_rules('description', translate('product_label_description_lang'), 'trim|required');

        if ($this->form_validation->run() == FALSE) {

            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            redirect("product/update_index/$product_id", "location", 301);
        }

        //$product = $this->product->get_by_id($product_id);

        // $product->sum_starts += $starts;

        // if ($starts != 0) {
        //     $product->total_score += 1;
        // }

        //$starts_average = $this->__get_starts_average($product->sum_starts, $product->total_score);

        $data_save_db = [
            'code' => (string) $code,
            'name' => (string) $name,
            'category' => (string) $category_id,
            'description' => (string) $description,
            'price_final' => (float) $price_final,
            'is_deal' => (int) $is_deal,
            'show_deal_banner' => (int) $is_show_deal_banner,
            'stock_quantity' => (int) $stock_quantity,
            //'sum_starts' => (int) $product->sum_starts,
            //'starts_average' => (int) $starts_average,
            //'total_score' => (int) $product->total_score,
            'views' => 0,
            'updated_at' => time(),
        ];

        $update_file = FALSE;

        $name_file = $_FILES['archivo']['name'];
        $ofile_name = explode('/', $old_file);

        if (end($ofile_name) != $name_file and $name_file != null) {
            $update_file = TRUE;
        }

        if (!$update_file) {

            $r = $this->product->update($product_id, $data_save_db);

            if ($r === false) {

                $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                redirect("product/index/", "location", 301);
            } else {

                $this->response->set_message(translate("message_update_product_ok"), ResponseMessage::SUCCESS);
                redirect("product/index/", "location", 301);
            }
        }

        $images_product = $this->__save_product_images($_FILES['archivo']['name'], $code . "_" . time());

        if (is_string($images_product)) {
            $this->response->set_message($images_product, ResponseMessage::ERROR);
            redirect("product/index", "location", 301);
        }

        if (is_array($images_product)) {
            $data_save_db['resource_main'] = $images_product['resource_main'];
            $data_save_db['resource_small'] = $images_product['resource_small'];
            $data_save_db['resource_catalog'] = $images_product['resource_catalog'];

            if ($old_file != null) {
                $old_file_small = str_replace('images_main', 'images_small', $old_file);
                $old_file_small = str_replace('.png', '_small.png', $old_file_small);
                $old_file_catalog = str_replace('images_main', 'images_aditional', $old_file);
                $old_file_catalog = str_replace('.png', '_catalog.png', $old_file_catalog);

                unlink($old_file);
                unlink($old_file_small);
                unlink($old_file_catalog);
            }

            $is_update = $this->product->update($product_id, $data_save_db);

            if ($is_update) {

                $this->response->set_message(translate("message_save_product_ok"), ResponseMessage::SUCCESS);
                redirect("product/index", "location", 301);
            } else {

                $this->response->set_message("Ocurrio un error al guardar el producto", ResponseMessage::ERROR);
                redirect("product/add_index", "location", 301);
            }
        }
    }



    public function delete($product_id = 0)
    {
        $this->validate_rol([1]);

        $product_object = $this->product->get_by_id($product_id);

        if ($product_object) {

            if (file_exists($product_object->resource_main)) {
                unlink($product_object->resource_main);
            }

            if (file_exists($product_object->resource_small)) {
                unlink($product_object->resource_small);
            }

            if (file_exists($product_object->resource_catalog)) {
                unlink($product_object->resource_catalog);
            }

            $this->product->delete($product_id);

            $this->response->set_message(translate('message_delete_product_ok'), ResponseMessage::SUCCESS);
            redirect("product/index", "location", 301);
        } else {

            $this->response->set_message(translate('message_error'), ResponseMessage::SUCCESS);
            redirect("product/index", "location", 301);
        }
    }


    public function get_detail()
    {
        $product_id = $this->input->post('id');

        $product = $this->product->get_by_id($product_id);

        if (!$product) {
            echo json_encode(['status' => FALSE, 'msg' => 'Ocurrio un problema al buscar el producto. Por favor intente nuevamente.']);
            return 0;
        }

        echo json_encode($product);
        return 0;
    }


    private function __create_directory($path_directory)
    {

        if (!is_dir($path_directory)) {

            if (!mkdir($path_directory, DIR_READ_MODE, TRUE)) {
                return FALSE;
            }
        }
        return TRUE;
    }


    private function __save_product_images($name_umpload_file, $name_save_file, $is_aditional_image = FALSE, $name_key_file = '')
    {
        if ($is_aditional_image && empty($name_key_file)) {
            return 'name_key_file is not defined';
        }

        if ($is_aditional_image == FALSE && $_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE) {
            return translate('message_file_size_exceeded_php_ini');
        }

        if ($is_aditional_image == TRUE && $_FILES[$name_key_file]['error'] == UPLOAD_ERR_INI_SIZE) {
            return translate('message_file_size_exceeded_php_ini');
        }

        $explode_name = explode('.', $name_umpload_file);
        $extension = end($explode_name);
        $is_valid_extension = in_array($extension, ALLOW_EXTENSION_UPLOAD_IMAGE);

        if (!$is_valid_extension) {
            return translate("message_file_tipe_not_accept");
        }

        $name_save_file_small = $name_save_file . "_small";
        $name_save_file_catalog = $name_save_file . "_catalog";

        $this->__create_directory($this->path_images_product);

        $data_return = [
            'resource_main' => '',
            'resource_small' => '',
            'resource_catalog' => '',
        ];

        if ($is_aditional_image == FALSE) {

            $result_img_main = save_image_from_post('archivo', $this->path_images_product_main, $name_save_file, $this::WIDTH, $this::HEIGHT);

            $result_img_small = save_image_from_post('archivo', $this->path_images_product_small, $name_save_file_small, $this::WIDTH_SMALL, $this::HEIGHT_SMALL);

            $result_img_catalog = save_image_from_post('archivo', $this->path_images_product_aditional, $name_save_file_catalog, $this::WIDTH_CATALOG, $this::HEIGHT_CATALOG);

            if ($result_img_main[0] && $result_img_small[0] && $result_img_catalog[0]) {

                $data_return['resource_main'] = str_replace(FCPATH, './', $result_img_main[1]);
                $data_return['resource_small'] = str_replace(FCPATH, './', $result_img_small[1]);
                $data_return['resource_catalog'] = str_replace(FCPATH, './', $result_img_catalog[1]);

                return $data_return;
            } else {

                return $result_img_main[1];
            }
        } else {

            $result_img_aditional = save_image_from_post($name_key_file, $this->path_images_product_aditional, $name_save_file, $this::WIDTH, $this::HEIGHT);

            $result_img_small = save_image_from_post($name_key_file, $this->path_images_product_small, $name_save_file_small, $this::WIDTH_SMALL, $this::HEIGHT_SMALL);

            if ($result_img_aditional[0] && $result_img_small[0]) {

                $data_return['resource_aditional'] = str_replace(FCPATH, './', $result_img_aditional[1]);
                $data_return['resource_small'] = str_replace(FCPATH, './', $result_img_small[1]);

                return $data_return;
            } else {

                return $result_img_main[1];
            }
        }
    }



    private function __get_starts_average($sum_starts = 0, $total_score = 0)
    {

        if ($sum_starts === 0) {
            return 0;
        }

        if ($total_score === 0) {
            $total_score = 1;
        }

        return (int) $sum_starts / $total_score;
    }
}
